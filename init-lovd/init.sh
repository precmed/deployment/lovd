#!/bin/bash

echo "checking database connection..."

for i in {1..100}; do
  if mysqladmin -h ${LOVD_DB_HOST} -u $MYSQL_USER -p$MYSQL_PASSWORD ping --silent &> /dev/null ; then
    echo "connection successful"
    break
  else
    echo "waiting for database connection..."
    sleep 1
  fi
done

set -e

SQL_OUTPUT=$(mysql -N -h ${LOVD_DB_HOST} -u $MYSQL_USER -p$MYSQL_PASSWORD -D ${LOVD_DB_NAME} -e 'SHOW TABLES LIKE "lovd_config"')

echo "checking if init is needed..."

if [ "$SQL_OUTPUT" != "lovd_config" ] || [ "$FORCE_INIT" = "true" ]; then
  echo "writing lovd configuration..."
  mysql -h ${LOVD_DB_HOST} -u ${MYSQL_USER} -p${MYSQL_PASSWORD} -D ${LOVD_DB_NAME} < lovd-db-installation.sql
  echo "done"
else
  echo "lovd_config already exists"
  echo "FORCE_INIT=${FORCE_INIT}"
  echo "no need to init mysql with lovd configuration"
fi
