-- MySQL dump 10.13  Distrib 5.7.33, for Linux (x86_64)
--
-- Host: mysql-lovd-testers    Database: lovd
-- ------------------------------------------------------
-- Server version 5.6.50

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lovd_active_columns`
--

-- COMMENTED OUT ALLELE FOREIGN KEY ON LOVD_VARIANTS TABLE
-- ADDED FORMAT & INFO FIELDS ON VARIANTS TABLE
-- ADDED VEP COLUMNS ON TRANSCRIPT TABLE
-- DELETED UNIQUE CONSTRAINT KEY ON TRANSCRIPTS.NCBI
-- UNIQUE CONSTRAINT REMOVED FROM lovd_genes.ncbi_id AS WELL AS NOT NULL TURNED TO DEFAULT NULL



DROP TABLE IF EXISTS `lovd_active_columns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_active_columns` (
  `colid` varchar(100) NOT NULL,
  `created_by` smallint(5)  DEFAULT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`colid`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `lovd_active_columns_fk_colid` FOREIGN KEY (`colid`) REFERENCES `lovd_columns` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lovd_active_columns_fk_created_by` FOREIGN KEY (`created_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Dumping data for table `lovd_active_columns`
--

LOCK TABLES `lovd_active_columns` WRITE;
/*!40000 ALTER TABLE `lovd_active_columns` DISABLE KEYS */;
INSERT INTO `lovd_active_columns` VALUES ('Individual/Lab_ID',00000,'2020-03-31 08:01:54'),('Individual/Reference',00000,'2020-03-31 08:01:54'),('Individual/Remarks',00000,'2020-03-31 08:01:55'),('Individual/Remarks_Non_Public',00000,'2020-03-31 08:01:55'),('Phenotype/Additional',00000,'2020-03-31 08:01:56'),('Phenotype/Age',00000,'2020-03-31 08:01:59'),('Phenotype/Inheritance',00000,'2020-03-31 08:01:56'),('Screening/Technique',00000,'2020-03-31 08:01:56'),('Screening/Template',00000,'2020-03-31 08:01:57'),('VariantOnGenome/DBID',00000,'2020-03-31 08:01:57'),('VariantOnGenome/DNA',00000,'2020-03-31 08:01:57'),('VariantOnGenome/Reference',00000,'2020-03-31 08:01:58'),('VariantOnTranscript/DNA',00000,'2020-03-31 08:01:58'),('VariantOnTranscript/Exon',00000,'2020-03-31 08:01:58'),('VariantOnTranscript/Protein',00000,'2020-03-31 08:01:59'),('VariantOnTranscript/RNA',00000,'2020-03-31 08:01:59');
/*!40000 ALTER TABLE `lovd_active_columns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_alleles`
--

DROP TABLE IF EXISTS `lovd_alleles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_alleles` (
  `id` tinyint(2) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `display_order` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_alleles`
--

LOCK TABLES `lovd_alleles` WRITE;
/*!40000 ALTER TABLE `lovd_alleles` DISABLE KEYS */;
INSERT INTO `lovd_alleles` VALUES (0,'Unknown',1),(1,'Parent #1',6),(2,'Parent #2',7),(3,'Both (homozygous)',8),(10,'Paternal (inferred)',3),(11,'Paternal (confirmed)',2),(20,'Maternal (inferred)',5),(21,'Maternal (confirmed)',4);
/*!40000 ALTER TABLE `lovd_alleles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_announcements`
--

DROP TABLE IF EXISTS `lovd_announcements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_announcements` (
  `id` smallint(5)  NOT NULL AUTO_INCREMENT,
  `type` varchar(15) NOT NULL DEFAULT 'information',
  `announcement` text NOT NULL,
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '9999-12-31 23:59:59',
  `lovd_read_only` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` smallint(5)  DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `edited_by` smallint(5)  DEFAULT NULL,
  `edited_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `edited_by` (`edited_by`),
  CONSTRAINT `lovd_announcements_fk_created_by` FOREIGN KEY (`created_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_announcements_fk_edited_by` FOREIGN KEY (`edited_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_announcements`
--

LOCK TABLES `lovd_announcements` WRITE;
/*!40000 ALTER TABLE `lovd_announcements` DISABLE KEYS */;
/*!40000 ALTER TABLE `lovd_announcements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_chromosomes`
--

DROP TABLE IF EXISTS `lovd_chromosomes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_chromosomes` (
  `name` varchar(2) NOT NULL,
  `sort_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `hg18_id_ncbi` varchar(20) NOT NULL,
  `hg19_id_ncbi` varchar(20) NOT NULL,
  `hg38_id_ncbi` varchar(20) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_chromosomes`
--

LOCK TABLES `lovd_chromosomes` WRITE;
/*!40000 ALTER TABLE `lovd_chromosomes` DISABLE KEYS */;
INSERT INTO `lovd_chromosomes` VALUES ('1',1,'NC_000001.9','NC_000001.10','NC_000001.11'),('10',10,'NC_000010.9','NC_000010.10','NC_000010.11'),('11',11,'NC_000011.8','NC_000011.9','NC_000011.10'),('12',12,'NC_000012.10','NC_000012.11','NC_000012.12'),('13',13,'NC_000013.9','NC_000013.10','NC_000013.11'),('14',14,'NC_000014.7','NC_000014.8','NC_000014.9'),('15',15,'NC_000015.8','NC_000015.9','NC_000015.10'),('16',16,'NC_000016.8','NC_000016.9','NC_000016.10'),('17',17,'NC_000017.9','NC_000017.10','NC_000017.11'),('18',18,'NC_000018.8','NC_000018.9','NC_000018.10'),('19',19,'NC_000019.8','NC_000019.9','NC_000019.10'),('2',2,'NC_000002.10','NC_000002.11','NC_000002.12'),('20',20,'NC_000020.9','NC_000020.10','NC_000020.11'),('21',21,'NC_000021.7','NC_000021.8','NC_000021.9'),('22',22,'NC_000022.9','NC_000022.10','NC_000022.11'),('3',3,'NC_000003.10','NC_000003.11','NC_000003.12'),('4',4,'NC_000004.10','NC_000004.11','NC_000004.12'),('5',5,'NC_000005.8','NC_000005.9','NC_000005.10'),('6',6,'NC_000006.10','NC_000006.11','NC_000006.12'),('7',7,'NC_000007.12','NC_000007.13','NC_000007.14'),('8',8,'NC_000008.9','NC_000008.10','NC_000008.11'),('9',9,'NC_000009.10','NC_000009.11','NC_000009.12'),('M',25,'NC_001807.4','NC_012920.1','NC_012920.1'),('X',23,'NC_000023.9','NC_000023.10','NC_000023.11'),('Y',24,'NC_000024.8','NC_000024.9','NC_000024.10');
/*!40000 ALTER TABLE `lovd_chromosomes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_colleagues`
--

DROP TABLE IF EXISTS `lovd_colleagues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_colleagues` (
  `userid_from` smallint(5)  NOT NULL,
  `userid_to` smallint(5)  NOT NULL,
  `allow_edit` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid_from`,`userid_to`),
  KEY `userid_to` (`userid_to`),
  CONSTRAINT `lovd_colleagues_fk_userid_from` FOREIGN KEY (`userid_from`) REFERENCES `lovd_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lovd_colleagues_fk_userid_to` FOREIGN KEY (`userid_to`) REFERENCES `lovd_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_colleagues`
--

LOCK TABLES `lovd_colleagues` WRITE;
/*!40000 ALTER TABLE `lovd_colleagues` DISABLE KEYS */;
/*!40000 ALTER TABLE `lovd_colleagues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_columns`
--

DROP TABLE IF EXISTS `lovd_columns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_columns` (
  `id` varchar(100) NOT NULL,
  `col_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `width` smallint(5) unsigned NOT NULL DEFAULT '50',
  `hgvs` tinyint(1) NOT NULL DEFAULT '0',
  `standard` tinyint(1) NOT NULL DEFAULT '0',
  `mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `head_column` varchar(50) NOT NULL,
  `description_form` text,
  `description_legend_short` text,
  `description_legend_full` text,
  `mysql_type` varchar(255) NOT NULL DEFAULT 'VARCHAR(50)',
  `form_type` text,
  `select_options` text,
  `preg_pattern` varchar(255) NOT NULL DEFAULT '',
  `public_view` tinyint(1) NOT NULL DEFAULT '1',
  `public_add` tinyint(1) NOT NULL DEFAULT '1',
  `allow_count_all` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` smallint(5)  DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `edited_by` smallint(5)  DEFAULT NULL,
  `edited_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `edited_by` (`edited_by`),
  CONSTRAINT `lovd_columns_fk_created_by` FOREIGN KEY (`created_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_columns_fk_edited_by` FOREIGN KEY (`edited_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_columns`
--

LOCK TABLES `lovd_columns` WRITE;
/*!40000 ALTER TABLE `lovd_columns` DISABLE KEYS */;
INSERT INTO `lovd_columns` VALUES ('Individual/Age_of_death',248,100,0,0,0,'Age of death','Type 35y for 35 years, 04y08m for 4 years and 8 months, 18y? for around 18 years, >54y for still alive at 55, ? for unknown.','The age at which the individual deceased, if known and applicable. 04y08m = 4 years and 8 months.','The age at which the individual deceased, if known and applicable.\r\n<UL style=\"margin-top:0px;\">\r\n  <LI>35y = 35 years</LI>\r\n  <LI>04y08m = 4 years and 8 months</LI>\r\n  <LI>18y? = around 18 years</LI>\r\n  <LI>&gt;54y = still alive at 55</LI>\r\n  <LI>? = unknown</LI>\r\n</UL>','VARCHAR(12)','Age of death|The age at which the individual deceased, if known and applicable. Numbers lower than 10 should be prefixed by a zero and the field should always begin with years, to facilitate sorting on this column.|text|10','','/^([<>]?\\d{2,3}y(\\d{2}m(\\d{2}d)?)?)?\\??$/',1,1,1,00000,'2020-03-31 08:01:54',NULL,NULL),('Individual/Consanguinity',240,40,0,0,0,'Consanguinity','Indicates whether the parents are related (consanguineous), not related (non-consanguineous) or whether consanguinity is not known (unknown)','Indicates whether the parents are related (consanguineous), not related (non-consanguineous) or whether consanguinity is not known (unknown)','Indicates whether the parents are related (consanguineous), not related (non-consanguineous) or whether consanguinity is not known (unknown)','VARCHAR(100)','Consanguinity||select|1|--Not specified--|false|false','? = Unknown\r\nno = Non-consanguineous parents\r\nyes = Consanguineous parents','',1,1,1,00000,'2020-03-31 08:01:54',NULL,NULL),('Individual/Death/Cause',249,150,0,0,0,'Cause of death','','The cause of the individual\'s death, if known and applicable.','The cause of the individual\'s death, if known and applicable.','VARCHAR(255)','Cause of death|The cause of the individual\'s death, if known and applicable.|text|30','','',1,1,1,00000,'2020-03-31 08:01:54',NULL,NULL),('Individual/Gender',3,70,0,0,0,'Gender','','Individual\'s gender.','The gender of the reported individual.','VARCHAR(100)','Gender||select|1|--Not specified--|false|false','? = Unknown\r\nF = Female\r\nM = Male\r\nrF = Raised as female\r\nrM = Raised as male','',1,1,1,00000,'2020-03-31 08:01:54',NULL,NULL),('Individual/Lab_ID',1,80,1,1,1,'Lab-ID','','The individual\'s ID in the hospital, diagnostic laboratory or in the publication.','The individual\'s ID in the hospital, diagnostic laboratory or in the publication.','VARCHAR(50)','Lab ID||text|30','','',0,1,0,00000,'2020-03-31 08:01:54',NULL,NULL),('Individual/Origin/Ethnic',201,200,0,0,0,'Ethnic origin','Ethnic origin of individual; e.g. African, Caucasian, gypsy, jew (Ashkenazi).','Ethnic origin of individual; e.g. African, Caucasian, gypsy, jew (Ashkenazi).','The ethnic origin of the individual; e.g. African, Caucasian, gypsy, jew (Ashkenazi).','VARCHAR(50)','Ethnic origin|If mixed, please indicate origin of father and mother, if known.|text|20','','',1,1,1,00000,'2020-03-31 08:01:54',NULL,NULL),('Individual/Origin/Geographic',200,200,0,0,0,'Geographic origin','Geographic origin of individual; Belgium = individual\'s origin is Belgium, (France) = reported by laboratory in France, individual\'s origin not sure.','Geographic origin of individual; Belgium = individual\'s origin is Belgium, (France) = reported by laboratory in France, individual\'s origin not sure.','The geographic origin of the individual (country and/or region); Belgium = individual\'s origin is Belgium, (France) = reported by laboratory in France, individual\'s origin not sure.','VARCHAR(50)','Geographic origin|If mixed, please indicate origin of father and mother, if known.|text|30','','',1,1,1,00000,'2020-03-31 08:01:54',NULL,NULL),('Individual/Origin/Population',202,200,0,0,0,'Population','','Individual population.','Additional information on the individual\'s population.','VARCHAR(50)','Individual population||text|30','','',1,1,1,00000,'2020-03-31 08:01:54',NULL,NULL),('Individual/Reference',2,200,1,1,0,'Reference','','Reference to publication describing the individual/family.','Reference to publication describing the individual/family, possibly giving more phenotypic details than listed in this database entry, including link to PubMed or other source, e.g. \"den Dunnen ASHG2003 P2346\". References in the &quot;Country:City&quot; format indicate that the variant was submitted directly to this database by the laboratory indicated.','VARCHAR(200)','Reference||text|50','','',1,1,1,00000,'2020-03-31 08:01:54',NULL,NULL),('Individual/Remarks',250,200,0,1,0,'Remarks','','Remarks about the individual.','Remarks about the individual.','TEXT','Remarks|Only provide data that do not belong in any other field.|textarea|50|3','','',1,1,1,00000,'2020-03-31 08:01:55',NULL,NULL),('Individual/Remarks_Non_Public',251,200,0,1,0,'Remarks (non public)','','Non-public remarks about the individual.','Non-public remarks about the individual.','TEXT','Remarks (non public)||textarea|50|3','','',0,0,0,00000,'2020-03-31 08:01:55',NULL,NULL),('Phenotype/Additional',250,200,0,1,0,'Phenotype details','Additional information on the phenotype of the individual.','Additional information on the phenotype of the individual.','Additional information on the phenotype of the individual.','TEXT','Additional phenotype details||textarea|40|4','','',1,1,1,00000,'2020-03-31 08:01:56',NULL,NULL),('Phenotype/Age',10,100,0,0,0,'Age examined','Type 35y for 35 years, 04y08m for 4 years and 8 months, 18y? for around 18 years, >54y for older than 54, ? for unknown.','The age at which the individual was examined, if known. 04y08m = 4 years and 8 months.','The age at which the individual was examined, if known.\r\n<UL style=\"margin-top:0px;\">\r\n  <LI>35y = 35 years</LI>\r\n  <LI>04y08m = 4 years and 8 months</LI>\r\n  <LI>18y? = around 18 years</LI>\r\n  <LI>&gt;54y = older than 54</LI>\r\n  <LI>? = unknown</LI>\r\n</UL>','VARCHAR(12)','Age at examination|The age at which the individual was examined, if known. Numbers lower than 10 should be prefixed by a zero and the field should always begin with years, to facilitate sorting on this column.|text|10','','/^([<>]?\\d{2,3}y(\\d{2}m(\\d{2}d)?)?)?\\??$/',1,1,1,00000,'2020-03-31 08:01:56',NULL,NULL),('Phenotype/Age/Onset',1,100,0,0,0,'Age of onset','Type 35y for 35 years, 04y08m for 4 years and 8 months, 18y? for around 18 years, >54y for older than 54, ? for unknown.','The age at which the first symptoms of the disease appeared in the individual, if known. 04y08m = 4 years and 8 months.','The age at which the first symptoms appeared in the individual, if known.\r\n<UL style=\"margin-top:0px;\">\r\n  <LI>35y = 35 years</LI>\r\n  <LI>04y08m = 4 years and 8 months</LI>\r\n  <LI>18y? = around 18 years</LI>\r\n  <LI>&gt;54y = older than 54</LI>\r\n  <LI>? = unknown</LI>\r\n</UL>','VARCHAR(12)','Age of onset|The age at which the first symptoms appeared in the individual, if known. Numbers lower than 10 should be prefixed by a zero and the field should always begin with years, to facilitate sorting on this column.|text|10','','/^([<>]?\\d{2,3}y(\\d{2}m(\\d{2}d)?)?)?\\??$/',1,1,1,00000,'2020-03-31 08:01:56',NULL,NULL),('Phenotype/Date',5,80,0,0,0,'Date','Format: YYYY-MM-DD.','Date the phenotype was observed.','Date the phenotype was observed, in YYYY-MM-DD format.','DATE','Date||text|10','','',1,1,1,00000,'2020-03-31 08:01:56',NULL,NULL),('Phenotype/Inheritance',254,200,1,1,0,'Inheritance','Indicates the inheritance of the phenotype in the family; unknown, familial (autosomal/X-linked, dominant/ recessive), paternal (Y-linked), maternal (mitochondrial), isolated (sporadic) or complex','Indicates the inheritance of the phenotype in the family; unknown, familial (autosomal/X-linked, dominant/ recessive), paternal (Y-linked), maternal (mitochondrial), isolated (sporadic) or complex','Indicates the inheritance of the phenotype in the family; unknown, familial (autosomal/X-linked, dominant/ recessive), paternal (Y-linked), maternal (mitochondrial), isolated (sporadic) or complex','VARCHAR(50)','Inheritance||select|1|--Not specified--|false|false','Unknown\r\nFamilial\r\nFamilial, autosomal dominant\r\nFamilial, autosomal recessive\r\nFamilial, X-linked dominant\r\nFamilial, X-linked dominant, male sparing\r\nFamilial, X-linked recessive\r\nPaternal, Y-linked\r\nMaternal, mitochondrial\r\nIsolated (sporadic)\r\nComplex','',1,1,1,00000,'2020-03-31 08:01:56',NULL,NULL),('Phenotype/Length',200,100,0,0,0,'Length','','Length of the individual, in cm.','Length of the individual, in centimeters (cm).','SMALLINT(3) UNSIGNED','Length of individual (cm)|Length of individual, in centimeters.|text|3','','',1,1,1,00000,'2020-03-31 08:01:56',NULL,NULL),('Screening/Date',1,80,0,0,0,'Date','Format: YYYY-MM-DD.','Date the detection technique was performed.','Date the detection technique was performed, in YYYY-MM-DD format.','DATE','Date||text|10','','',1,1,1,00000,'2020-03-31 08:01:56',NULL,NULL),('Screening/Technique',3,200,1,1,1,'Technique','Technique(s) used to identify the sequence variant; select multiple when more were used. For SEQ-NG, please indicate in the variant\'s remarks field the number of reads showing the variant (e.g. 47/96 reads, 123/123 reads), and create a second screening if you confirmed a variant using another method (e.g. SEQ).','Technique(s) used to identify the sequence variant.','Technique(s) used to identify the sequence variant.','TEXT','Technique(s) used||select|5|false|true|false','? = Unknown\r\narrayCGH = array for Comparative Genomic Hybridisation\r\narraySEQ = array for resequencing\r\narraySNP = array for SNP typing\r\narrayCNV = array for Copy Number Variation (SNP and CNV probes)\r\nBESS = Base Excision Sequence Scanning\r\nCMC = Chemical Mismatch Cleavage\r\nCSCE = Conformation Sensitive Capillary Electrophoresis\r\nDGGE = Denaturing-Gradient Gel-Electrophoresis\r\nDHPLC = Denaturing High-Performance Liquid Chromatography\r\nDOVAM = Detection Of Virtually All Mutations (SSCA variant)\r\nddF = dideoxy Fingerprinting\r\nDSCA = Double-Strand DNA Conformation Analysis\r\nEMC = Enzymatic Mismatch Cleavage\r\nHD = HeteroDuplex analysis\r\nMCA = high-resolution Melting Curve Analysis (hrMCA)\r\nIHC = Immuno-Histo-Chemistry\r\nMAPH = Multiplex Amplifiable Probe Hybridisation\r\nMLPA = Multiplex Ligation-dependent Probe Amplification\r\nSEQ-NG = Next-Generation Sequencing\r\nSEQ-NG-H = Next-Generation Sequencing - Helicos\r\nSEQ-NG-I = Next-Generation Sequencing - Illumina/Solexa\r\nSEQ-NG-R = Next-Generation Sequencing - Roche/454\r\nSEQ-NG-S = Next-Generation Sequencing - SOLiD\r\nNorthern = Northern blotting\r\nPCR = Polymerase Chain Reaction\r\nPCRdig = PCR + restriction enzyme digestion\r\nPCRlr = PCR, long-range\r\nPCRm = PCR, multiplex\r\nPCRq = PCR, quantitative\r\nPAGE = Poly-Acrylamide Gel-Electrophoresis\r\nPTT = Protein Truncation Test\r\nPFGE = Pulsed-Field Gel-Electrophoresis (+Southern)\r\nRT-PCR = Reverse Transcription and PCR\r\nSEQ = SEQuencing\r\nSBE = Single Base Extension\r\nSSCA = Single-Strand DNA Conformation polymorphism Analysis (SSCP)\r\nSSCAf = SSCA, fluorescent (SSCP)\r\nSouthern = Southern blotting\r\nTaqMan = TaqMan assay\r\nWestern = Western Blotting','',1,1,1,00000,'2020-03-31 08:01:56',NULL,NULL),('Screening/Template',2,80,1,1,1,'Template','','Template(s) used to detect the sequence variant; DNA = genomic DNA, RNA = RNA (cDNA).','Template(s) used to detect the sequence variant; DNA = genomic DNA, RNA = RNA (cDNA).','TEXT','Detection template||select|3|false|true|false','DNA\r\nRNA = RNA (cDNA)\r\nProtein\r\n? = unknown','',1,1,1,00000,'2020-03-31 08:01:57',NULL,NULL),('Screening/Tissue',4,100,0,0,1,'Tissue','','Tissue type used for the detection of sequence variants.','Tissue type used for the detection of sequence variants.','VARCHAR(25)','Tissue||text|20','','',1,1,1,00000,'2020-03-31 08:01:57',NULL,NULL),('VariantOnGenome/Conservation_score/GERP',4,100,0,0,0,'GERP conservation','','Conservation score as calculated by GERP.','The Conservation score as calculated by GERP.','DECIMAL(5,3)','GERP conservation score||text|6','','',1,1,1,00000,'2020-03-31 08:01:57',NULL,NULL),('VariantOnGenome/DBID',7,120,1,1,1,'DB-ID','NOTE: This field will be predicted and filled in by LOVD, if left empty.','Database ID of variant starting with the HGNC gene symbol, followed by an underscore (_) and a six digit number (e.g. DMD_012345). _000000 is used for variants where DNA was not analysed (change predicted from RNA analysis), variants seen in animal models or variants not seen in humans but functionally tested in vitro.','Database ID of variant, grouping multiple observations of the same variant together, starting with the HGNC gene symbol, followed by an underscore (_) and a six digit number (e.g. DMD_012345). _000000 is used for variants where DNA was not analysed (change predicted from RNA analysis), variants seen in animal models or variants not seen in humans but functionally tested in vitro.','VARCHAR(50)','ID|This ID is used to group multiple observations of the same variant together. This field will be predicted and filled in by LOVD when left empty. The ID starts with the HGNC gene symbol of the transcript most influenced by the variant or otherwise the closest gene, followed by an underscore (_) and a six digit number (e.g. DMD_012345). _000000 is used for variants where DNA was not analysed (change predicted from RNA analysis), variants seen in animal models or variants not seen in humans but functionally tested in vitro.|text|20','','/^(chr(\\d{1,2}|[XYM])|(C(\\d{1,2}|[XYM])orf[\\d][\\dA-Z]*-|[A-Z][A-Z0-9]*-)?(C(\\d{1,2}|[XYM])orf[\\d][\\dA-Z]*|[A-Z][A-Z0-9-]*))_\\d{6}$/',1,0,1,00000,'2020-03-31 08:01:57',NULL,NULL),('VariantOnGenome/dbSNP',8,120,0,0,0,'dbSNP ID','','The dbSNP ID.','The dbSNP ID.','VARCHAR(15)','dbSNP ID|If available, please fill in the dbSNP ID, such as rs12345678.|text|10','','/^[rs]s\\d+$/',1,1,1,00000,'2020-03-31 08:01:57',NULL,NULL),('VariantOnGenome/DNA',2,200,1,1,1,'DNA change (genomic)','','Description of variant at DNA level, based on the genomic DNA reference sequence (following HGVS recommendations).','Description of variant at DNA level, based on the genomic DNA reference sequence (following HGVS recommendations).<BR>\r\n<UL style=\"margin-top : 0px;\">\r\n  <LI>g.12345678C>T</LI>\r\n  <LI>g.12345678_12345890del</LI>\r\n  <LI>g.12345678_12345890dup</LI>\r\n</UL>','VARCHAR(255)','Genomic DNA change (HGVS format)|Description of variant at DNA level, based on the genomic DNA reference sequence (following HGVS recommendations); e.g. g.12345678C>T, g.12345678_12345890del, g.12345678_12345890dup.|text|30','','',1,1,1,00000,'2020-03-31 08:01:57',NULL,NULL),('VariantOnGenome/Frequency',9,90,0,0,0,'Frequency','','Frequency in which the variant was found; e.g 5/760 chromosomes (in 5 of 760 chromosomes tested), 1/33 patients (in 1 of 33 patients analysed in study), 0.05 controls (in 5% of control cases tested).','Frequency in which the variant was found; e.g 5/760 chromosomes (in 5 of 760 chromosomes tested), 1/33 patients (in 1 of 33 patients analysed in study), 0.05 controls (in 5% of control cases tested).','VARCHAR(15)','Frequency|Frequency in which the variant was found; e.g 5/760 chromosomes (in 5 of 760 chromosomes tested), 1/33 patients (in 1 of 33 patients analysed in study), 0.05 controls (in 5% of control cases tested). Preferred format is 3/75, not 0.04.|text|10','','',1,1,1,00000,'2020-03-31 08:01:57',NULL,NULL),('VariantOnGenome/Genetic_origin',11,200,0,0,1,'Genetic origin','','Origin of variant; unknown, germline, somatic, de novo, from parental disomy (maternal or paternal) or in vitro (cloned) when tested for functional consequences.','Origin of variant; unknown, germline, somatic, de novo, from parental disomy (maternal or paternal) or in vitro (cloned) when tested for functional consequences.','VARCHAR(100)','Genetic origin||select|1|--Not specified--|false|false','Unknown\r\nGermline\r\nSomatic\r\nDe novo\r\nUniparental disomy\r\nUniparental disomy, maternal allele\r\nUniparental disomy, paternal allele\r\nIn vitro (cloned)','',1,1,1,00000,'2020-03-31 08:01:58',NULL,NULL),('VariantOnGenome/Published_as',3,200,0,0,0,'Published as','Variant as originally reported (e.g. 521delT); provide only when different from \"DNA change\".','Variant as originally reported (e.g. 521delT); listed only when different from \"DNA change\". Variants seen in animal models, tested in vitro, predicted from RNA analysis, etc. are described between brackets like c.(456C>G).','Variant as originally reported (e.g. 521delT); listed only when different from \"DNA change\". Variants seen in animal models, tested in vitro, predicted from RNA analysis, etc. are described between brackets like c.(456C>G).','VARCHAR(100)','Published as|Variants seen in animal models, tested in vitro, predicted from RNA analysis, etc. are described between brackets like c.(456C>G).|text|30','','',1,1,1,00000,'2020-03-31 08:01:58',NULL,NULL),('VariantOnGenome/Reference',6,200,1,1,0,'Reference','','Reference to publication describing the variant.','Reference to publication describing the variant, including links to OMIM (when available), PubMed or or other source, e.g. \"den Dunnen ASHG2003 P2346\".','VARCHAR(255)','Reference||text|50','','',1,1,1,00000,'2020-03-31 08:01:58',NULL,NULL),('VariantOnGenome/Remarks',10,200,0,0,0,'Variant remarks','Remarks regarding the variant described, e.g. germline mosaicism in mother, 345 kb deletion, muscle RNA analysed, not in 200 control chromosomes tested, on founder haplotype, etc.','Remarks regarding the variant described.','Remarks regarding the variant described, e.g. germline mosaicism in mother, 345 kb deletion, muscle RNA analysed, not in 200 control chromosomes tested, on founder haplotype, etc.','TEXT','Remarks||textarea|50|3','','',1,1,1,00000,'2020-03-31 08:01:58',NULL,NULL),('VariantOnGenome/Restriction_site',5,75,0,0,0,'Re-site','','Restriction enzyme recognition site created (+) or destroyed (-).','Restriction enzyme recognition site created (+) or destroyed (-); e.g. BglII+, BamHI-.','VARCHAR(25)','Re-site|Restriction enzyme recognition site created (+) or destroyed (-); e.g. BglII+, BamHI-|text|10','','',1,1,1,00000,'2020-03-31 08:01:58',NULL,NULL),('VariantOnGenome/Segregation',12,40,0,0,0,'Segregation','Indicates whether the variant segregates with the phenotype (yes), does not segregate with the phenotype (no) or segregation is unknown (?)','Indicates whether the variant segregates with the phenotype (yes), does not segregate with the phenotype (no) or segregation is unknown (?)','Indicates whether the variant segregates with the phenotype (yes), does not segregate with the phenotype (no) or segregation is unknown (?)','VARCHAR(100)','Segregation||select|1|--Not specified--|false|false','? = Unknown\r\nyes = Segregates with phenotype\r\nno = Does not segregate with phenotype','',1,1,1,00000,'2020-03-31 08:01:58',NULL,NULL),('VariantOnGenome/Type',1,200,0,0,1,'Type','','Type of variant at DNA level.','Type of variant at DNA level; note that the variant type can also be derived from the variant description (for all levels).','VARCHAR(100)','Type of variant (DNA level)|Type of variant at DNA level; note that the variant type can also be derived from the variant description (for all levels).|select|1|true|false|false','Substitution\r\nDeletion\r\nDuplication\r\nInsertion\r\nInversion\r\nInsertion/Deletion\r\nTranslocation\r\nOther/Complex','',1,1,1,00000,'2020-03-31 08:01:58',NULL,NULL),('VariantOnTranscript/Distance_to_splice_site',10,150,0,0,0,'Splice distance','','The distance to the nearest splice site.','The distance to the nearest splice site.','MEDIUMINT(8) UNSIGNED','Distance to splice site||text|8','','',1,1,1,00000,'2020-03-31 08:01:58',NULL,NULL),('VariantOnTranscript/DNA',3,200,1,1,1,'DNA change (cDNA)','','Description of variant at DNA level, based on a coding DNA reference sequence (following HGVS recommendations).','Description of variant at DNA level, based on a coding DNA reference sequence (following HGVS recommendations); e.g. c.123C>T, c.123_145del, c.123_126dup.','VARCHAR(255)','DNA change (HGVS format)|Description of variant at DNA level, based on a coding DNA reference sequence (following HGVS recommendations); e.g. c.123C>T, c.123_145del, c.123_126dup.|text|30','','',1,1,1,00000,'2020-03-31 08:01:58',NULL,NULL),('VariantOnTranscript/Exon',2,50,0,1,0,'Exon','','Number of exon/intron containing the variant.','Number of exon/intron containing variant; 2 = exon 2, 12i = intron 12, 2i_7i = exons 3 to 7, 8i_9 = border intron 8/exon 9.','VARCHAR(7)','Exon|Format: 2 = exon 2, 12i = intron 12, 2i_7i = exons 3 to 7, 8i_9 = border intron 8/exon 9.|text|7','','',1,1,1,00000,'2020-03-31 08:01:58',NULL,NULL),('VariantOnTranscript/GVS/Function',9,200,0,0,0,'GVS function','','Functional annotation of this position from the Genome Variation Server.','The functional annotation of this position from the Genome Variation Server.','VARCHAR(100)','GVS function||select|1|true|false|false','intergenic\r\nnear-gene-5\r\nutr-5\r\nstart-lost\r\ncoding\r\nnon-coding-exon\r\ncoding-near-splice\r\nnon-coding-exon-near-splice\r\ncoding-synonymous\r\ncoding-synonymous-near-splice\r\ncodingComplex\r\ncodingComplex-near-splice\r\nframeshift\r\nframeshift-near-splice\r\nmissense\r\nmissense-near-splice\r\nsplice-5\r\nsplice\r\nnon-coding-intron-near-splice\r\nintron\r\nsplice-3\r\nstop-gained\r\nstop-gained-near-splice\r\nstop-lost\r\nstop-lost-near-splice\r\nutr-3\r\nnear-gene-3','',1,1,1,00000,'2020-03-31 08:01:58',NULL,NULL),('VariantOnTranscript/Location',1,200,0,0,1,'Location','','Location of variant at DNA level.','Location of variant at DNA level; note that the variant location can also be derived from the variant description.','VARCHAR(100)','Location of variant|The variant location can also be derived from the variant description|select|1|true|false|false','5\' gene flanking\r\n5\' UTR\r\nExon\r\nIntron\r\n3\' UTR\r\n3\' gene flanking','',1,1,1,00000,'2020-03-31 08:01:58',NULL,NULL),('VariantOnTranscript/PolyPhen',8,200,0,0,0,'PolyPhen prediction','','Effect of variant, predicted by PolyPhen.','Effect of variant, predicted by PolyPhen.','VARCHAR(100)','PolyPhen prediction|Effect of variant, predicted by PolyPhen|select|1|true|false|false','benign = Benign\r\npossiblyDamaging = Possibly damaging\r\nprobablyDamaging = Probably damaging\r\nnoPrediction = No prediction','',1,1,1,00000,'2020-03-31 08:01:58',NULL,NULL),('VariantOnTranscript/Position',5,100,0,0,0,'Position','','Position of variant in coding DNA sequence.','Position of variant in coding DNA sequence; note that coding DNA position can also be derived from the variant description.','MEDIUMINT(6)','cDNA position|Note that cDNA position can be derived from the variant description.|text|5','','',1,1,1,00000,'2020-03-31 08:01:58',NULL,NULL),('VariantOnTranscript/Protein',7,200,1,1,1,'Protein','','Description of variant at protein level (following HGVS recommendations).','Description of variant at protein level (following HGVS recommendations).<BR>\r\n<UL style=\"margin-top : 0px;\">\r\n  <LI>p.(Arg345Pro) = change predicted from DNA (RNA not analysed)</LI>\r\n  <LI>p.Arg345Pro = change derived from RNA analysis</LI>\r\n  <LI>p.? = unknown effect</LI>\r\n  <LI>p.0? = probably no protein produced</LI>\r\n</UL>','VARCHAR(255)','Protein change (HGVS format)|Description of variant at protein level (following HGVS recommendations); e.g. p.(Arg345Pro) = change predicted from DNA (RNA not analysed), p.Arg345Pro = change derived from RNA analysis, p.0 (no protein produced), p.? (unknown effect).|text|30','','',1,1,1,00000,'2020-03-31 08:01:59',NULL,NULL),('VariantOnTranscript/Published_as',4,200,0,0,0,'Published as','Variant as originally reported (e.g. 521delT); provide only when different from \"DNA change\".','Variant as originally reported (e.g. 521delT); listed only when different from \"DNA change\". Variants seen in animal models, tested in vitro, predicted from RNA analysis, etc. are described between brackets like c.(456C>G).','Variant as originally reported (e.g. 521delT); listed only when different from \"DNA change\". Variants seen in animal models, tested in vitro, predicted from RNA analysis, etc. are described between brackets like c.(456C>G).','VARCHAR(100)','Published as|Variants seen in animal models, tested in vitro, predicted from RNA analysis, etc. are described between brackets like c.(456C>G).|text|30','','',1,1,1,00000,'2020-03-31 08:01:59',NULL,NULL),('VariantOnTranscript/RNA',6,200,1,1,1,'RNA change','','Description of variant at RNA level (following HGVS recommendations).','Description of variant at RNA level (following HGVS recommendations).<BR>\r\n<UL style=\"margin-top : 0px;\">\r\n  <LI>r.123c>u</LI>\r\n  <LI>r.? = unknown</LI>\r\n  <LI>r.(?) = RNA not analysed but probably transcribed copy of DNA variant</LI>\r\n  <LI>r.spl? = RNA not analysed but variant probably affects splicing</LI>\r\n  <LI>r.(spl?) = RNA not analysed but variant may affect splicing</LI>\r\n  <LI>r.0? = change expected to abolish transcription</LI>\r\n</UL>','VARCHAR(100)','RNA change (HGVS format)|Description of variant at RNA level (following HGVS recommendations); e.g. r.123c>u, r.? = unknown, r.(?) = RNA not analysed but probably transcribed copy of DNA variant, r.spl? = RNA not analysed but variant probably affects splicing, r.(spl?) = RNA not analysed but variant may affect splicing.|text|30','','',1,1,1,00000,'2020-03-31 08:01:59',NULL,NULL);
/*!40000 ALTER TABLE `lovd_columns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_columns2links`
--

DROP TABLE IF EXISTS `lovd_columns2links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_columns2links` (
  `colid` varchar(100) NOT NULL,
  `linkid` tinyint(3)  NOT NULL,
  PRIMARY KEY (`colid`,`linkid`),
  KEY `linkid` (`linkid`),
  CONSTRAINT `lovd_columns2links_fk_colid` FOREIGN KEY (`colid`) REFERENCES `lovd_columns` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lovd_columns2links_fk_linkid` FOREIGN KEY (`linkid`) REFERENCES `lovd_links` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_columns2links`
--

LOCK TABLES `lovd_columns2links` WRITE;
/*!40000 ALTER TABLE `lovd_columns2links` DISABLE KEYS */;
INSERT INTO `lovd_columns2links` VALUES ('Individual/Reference',001),('VariantOnGenome/Reference',001),('VariantOnGenome/Reference',002),('VariantOnGenome/Reference',003),('VariantOnGenome/Reference',004),('Individual/Reference',005);
/*!40000 ALTER TABLE `lovd_columns2links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_config`
--

DROP TABLE IF EXISTS `lovd_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_config` (
  `system_title` varchar(255) NOT NULL DEFAULT 'LOVD - Leiden Open Variation Database',
  `institute` varchar(255) NOT NULL DEFAULT '',
  `location_url` varchar(255) NOT NULL DEFAULT '',
  `email_address` varchar(75) NOT NULL DEFAULT '',
  `send_admin_submissions` tinyint(1) NOT NULL DEFAULT '0',
  `api_feed_history` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `refseq_build` varchar(4) NOT NULL DEFAULT 'hg38',
  `proxy_host` varchar(255) NOT NULL DEFAULT '',
  `proxy_port` smallint(5) unsigned DEFAULT NULL,
  `proxy_username` varchar(255) NOT NULL DEFAULT '',
  `proxy_password` varchar(255) NOT NULL DEFAULT '',
  `logo_uri` varchar(100) NOT NULL DEFAULT 'gfx/LOVD3_logo145x50.jpg',
  `mutalyzer_soap_url` varchar(100) NOT NULL DEFAULT 'https://mutalyzer.nl/services',
  `omim_apikey` varchar(40) NOT NULL DEFAULT '',
  `send_stats` tinyint(1) NOT NULL DEFAULT '1',
  `include_in_listing` tinyint(1) NOT NULL DEFAULT '1',
  `allow_submitter_registration` tinyint(1) NOT NULL DEFAULT '1',
  `lock_users` tinyint(1) NOT NULL DEFAULT '1',
  `allow_unlock_accounts` tinyint(1) NOT NULL DEFAULT '1',
  `allow_submitter_mods` tinyint(1) NOT NULL DEFAULT '1',
  `allow_count_hidden_entries` tinyint(1) NOT NULL DEFAULT '0',
  `use_ssl` tinyint(1) NOT NULL DEFAULT '0',
  `use_versioning` tinyint(1) NOT NULL DEFAULT '0',
  `lock_uninstall` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_config`
--

LOCK TABLES `lovd_config` WRITE;
/*!40000 ALTER TABLE `lovd_config` DISABLE KEYS */;
INSERT INTO `lovd_config` VALUES ('LOVD - Leiden Open Variation Database','','https://lovd.testbed-precmed.iit.demokritos.gr','giannos.chatziagapis@gmail.com',0,0,'hg19','',NULL,'','','gfx/LOVD3_logo145x50.jpg','https://mutalyzer.nl/services','',0,0,0,1,1,1,0,0,0,1);
/*!40000 ALTER TABLE `lovd_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_countries`
--

DROP TABLE IF EXISTS `lovd_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_countries` (
  `id` char(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_countries`
--

LOCK TABLES `lovd_countries` WRITE;
/*!40000 ALTER TABLE `lovd_countries` DISABLE KEYS */;
INSERT INTO `lovd_countries` VALUES ('AD','Andorra'),('AE','United Arab Emirates'),('AF','Afghanistan'),('AG','Antigua and Barbuda'),('AI','Anguilla'),('AL','Albania'),('AM','Armenia'),('AN','Netherlands Antilles'),('AO','Angola'),('AQ','Antarctica'),('AR','Argentina'),('AS','American Samoa'),('AT','Austria'),('AU','Australia'),('AW','Aruba'),('AX','Åland Islands'),('AZ','Azerbaijan'),('BA','Bosnia and Herzegovina'),('BB','Barbados'),('BD','Bangladesh'),('BE','Belgium'),('BF','Burkina Faso'),('BG','Bulgaria'),('BH','Bahrain'),('BI','Burundi'),('BJ','Benin'),('BL','Saint Barthélemy'),('BM','Bermuda'),('BN','Brunei Darussalam'),('BO','Bolivia (Plurinational State of)'),('BR','Brazil'),('BS','Bahamas'),('BT','Bhutan'),('BV','Bouvet Island'),('BW','Botswana'),('BY','Belarus'),('BZ','Belize'),('CA','Canada'),('CC','Cocos (Keeling) Islands'),('CD','Congo (Democratic Republic of the)'),('CF','Central African Republic'),('CG','Congo'),('CH','Switzerland'),('CI','Côte D\'Ivoire (Ivory Coast)'),('CK','Cook Islands'),('CL','Chile'),('CM','Cameroon'),('CN','China'),('CO','Colombia'),('CR','Costa Rica'),('CU','Cuba'),('CV','Cape Verde'),('CX','Christmas Island'),('CY','Cyprus'),('CZ','Czech Republic'),('DE','Germany'),('DJ','Djibouti'),('DK','Denmark'),('DM','Dominica'),('DO','Dominican Republic'),('DZ','Algeria'),('EC','Ecuador'),('EE','Estonia'),('EG','Egypt'),('EH','Western Sahara'),('ER','Eritrea'),('ES','Spain'),('ET','Ethiopia'),('FI','Finland'),('FJ','Fiji'),('FK','Falkland Islands (Malvinas)'),('FM','Micronesia (Federated States of)'),('FO','Faroe Islands'),('FR','France'),('GA','Gabon'),('GB','United Kingdom (Great Britain)'),('GD','Grenada'),('GE','Georgia'),('GF','French Guiana'),('GG','Guernsey'),('GH','Ghana'),('GI','Gibraltar'),('GL','Greenland'),('GM','Gambia'),('GN','Guinea'),('GP','Guadeloupe'),('GQ','Equatorial Guinea'),('GR','Greece'),('GS','South Georgia and The South Sandwich Islands'),('GT','Guatemala'),('GU','Guam'),('GW','Guinea-Bissau'),('GY','Guyana'),('HK','Hong Kong'),('HM','Heard Island and McDonald Islands'),('HN','Honduras'),('HR','Croatia'),('HT','Haiti'),('HU','Hungary'),('ID','Indonesia'),('IE','Ireland'),('IL','Israel'),('IM','Isle of Man'),('IN','India'),('IO','British Indian Ocean Territory'),('IQ','Iraq'),('IR','Iran (Islamic Republic of)'),('IS','Iceland'),('IT','Italy'),('JE','Jersey'),('JM','Jamaica'),('JO','Jordan'),('JP','Japan'),('KE','Kenya'),('KG','Kyrgyzstan'),('KH','Cambodia'),('KI','Kiribati'),('KM','Comoros'),('KN','Saint Kitts and Nevis'),('KP','Korea (North) (Democratic People\'s Republic of)'),('KR','Korea (South) (Republic of)'),('KW','Kuwait'),('KY','Cayman Islands'),('KZ','Kazakhstan'),('LA','Lao People\'s Democratic Republic'),('LB','Lebanon'),('LC','Saint Lucia'),('LI','Liechtenstein'),('LK','Sri Lanka'),('LR','Liberia'),('LS','Lesotho'),('LT','Lithuania'),('LU','Luxembourg'),('LV','Latvia'),('LY','Libya'),('MA','Morocco'),('MC','Monaco'),('MD','Moldova (Republic of)'),('ME','Montenegro'),('MF','Saint Martin'),('MG','Madagascar'),('MH','Marshall Islands'),('MK','Macedonia (the former Yugoslav Republic of)'),('ML','Mali'),('MM','Myanmar'),('MN','Mongolia'),('MO','Macao'),('MP','Northern Mariana Islands'),('MQ','Martinique'),('MR','Mauritania'),('MS','Montserrat'),('MT','Malta'),('MU','Mauritius'),('MV','Maldives'),('MW','Malawi'),('MX','Mexico'),('MY','Malaysia'),('MZ','Mozambique'),('NA','Namibia'),('NC','New Caledonia'),('NE','Niger'),('NF','Norfolk Island'),('NG','Nigeria'),('NI','Nicaragua'),('NL','Netherlands'),('NO','Norway'),('NP','Nepal'),('NR','Nauru'),('NU','Niue'),('NZ','New Zealand'),('OM','Oman'),('PA','Panama'),('PE','Peru'),('PF','French Polynesia'),('PG','Papua New Guinea'),('PH','Philippines'),('PK','Pakistan'),('PL','Poland'),('PM','Saint Pierre and Miquelon'),('PN','Pitcairn'),('PR','Puerto Rico'),('PS','Palestinian Territory'),('PT','Portugal'),('PW','Palau'),('PY','Paraguay'),('QA','Qatar'),('RE','Réunion'),('RO','Romania'),('RS','Serbia'),('RU','Russian Federation'),('RW','Rwanda'),('SA','Saudi Arabia'),('SB','Solomon Islands'),('SC','Seychelles'),('SD','Sudan'),('SE','Sweden'),('SG','Singapore'),('SH','Saint Helena, Ascension and Tristan da Cunha'),('SI','Slovenia'),('SJ','Svalbard and Jan Mayen'),('SK','Slovakia'),('SL','Sierra Leone'),('SM','San Marino'),('SN','Senegal'),('SO','Somalia'),('SR','Suriname'),('SS','South Sudan'),('ST','Sao Tome and Principe'),('SV','El Salvador'),('SY','Syrian Arab Republic'),('SZ','Swaziland'),('TC','Turks and Caicos Islands'),('TD','Chad'),('TF','French Southern Territories'),('TG','Togo'),('TH','Thailand'),('TJ','Tajikistan'),('TK','Tokelau'),('TL','Timor-Leste'),('TM','Turkmenistan'),('TN','Tunisia'),('TO','Tonga'),('TR','Turkey'),('TT','Trinidad and Tobago'),('TV','Tuvalu'),('TW','Taiwan'),('TZ','Tanzania (United Republic of)'),('UA','Ukraine'),('UG','Uganda'),('UM','United States Minor Outlying Islands'),('US','United States'),('UY','Uruguay'),('UZ','Uzbekistan'),('VA','Holy See (Vatican City State)'),('VC','Saint Vincent and The Grenadines'),('VE','Venezuela (Bolivarian Republic of)'),('VG','Virgin Islands (British)'),('VI','Virgin Islands (U.S.)'),('VN','Viet Nam'),('VU','Vanuatu'),('WF','Wallis and Futuna'),('WS','Samoa'),('YE','Yemen'),('YT','Mayotte'),('ZA','South Africa'),('ZM','Zambia'),('ZW','Zimbabwe');
/*!40000 ALTER TABLE `lovd_countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_data_status`
--

DROP TABLE IF EXISTS `lovd_data_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_data_status` (
  `id` tinyint(1) unsigned NOT NULL,
  `name` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_data_status`
--

LOCK TABLES `lovd_data_status` WRITE;
/*!40000 ALTER TABLE `lovd_data_status` DISABLE KEYS */;
INSERT INTO `lovd_data_status` VALUES (1,'In progress'),(2,'Pending'),(4,'Non public'),(7,'Marked'),(9,'Public');
/*!40000 ALTER TABLE `lovd_data_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_diseases`
--

DROP TABLE IF EXISTS `lovd_diseases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_diseases` (
  `id` smallint(5)  NOT NULL AUTO_INCREMENT,
  `symbol` varchar(25) NOT NULL DEFAULT '-',
  `name` varchar(255) NOT NULL,
  `inheritance` varchar(45) DEFAULT NULL,
  `id_omim` int(10) unsigned DEFAULT NULL,
  `tissues` text,
  `features` text,
  `remarks` text,
  `created_by` smallint(5)  DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `edited_by` smallint(5)  DEFAULT NULL,
  `edited_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_omim` (`id_omim`),
  KEY `created_by` (`created_by`),
  KEY `edited_by` (`edited_by`),
  CONSTRAINT `lovd_diseases_fk_created_by` FOREIGN KEY (`created_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_diseases_fk_edited_by` FOREIGN KEY (`edited_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_diseases`
--

LOCK TABLES `lovd_diseases` WRITE;
/*!40000 ALTER TABLE `lovd_diseases` DISABLE KEYS */;
INSERT INTO `lovd_diseases` VALUES (00000,'Healthy/Control','Healthy individual / control',NULL,NULL,'','','',00000,'2020-03-31 08:01:59',NULL,NULL);
/*!40000 ALTER TABLE `lovd_diseases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_external_sources`
--

DROP TABLE IF EXISTS `lovd_external_sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_external_sources` (
  `id` varchar(15) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_external_sources`
--

LOCK TABLES `lovd_external_sources` WRITE;
/*!40000 ALTER TABLE `lovd_external_sources` DISABLE KEYS */;
INSERT INTO `lovd_external_sources` VALUES ('entrez','https://www.ncbi.nlm.nih.gov/gene?cmd=Retrieve&dopt=full_report&list_uids={{ ID }}'),('genbank','https://www.ncbi.nlm.nih.gov/nuccore/{{ ID }}'),('genecards','http://www.genecards.org/cgi-bin/carddisp.pl?gene={{ ID }}'),('genetests','https://www.ncbi.nlm.nih.gov/gtr/genes/{{ ID }}'),('hgmd','http://www.hgmd.cf.ac.uk/ac/gene.php?gene={{ ID }}'),('hgnc','https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/HGNC:{{ ID }}'),('hpo_disease','http://compbio.charite.de/hpoweb/showterm?disease=OMIM:{{ ID }}'),('omim','http://www.omim.org/entry/{{ ID }}'),('pubmed_article','https://www.ncbi.nlm.nih.gov/pubmed/{{ ID }}'),('pubmed_gene','https://www.ncbi.nlm.nih.gov/pubmed?LinkName=gene_pubmed&from_uid={{ ID }}'),('uniprot','http://www.uniprot.org/uniprot/{{ ID }}');
/*!40000 ALTER TABLE `lovd_external_sources` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Table structure for table `lovd_genes`
--

DROP TABLE IF EXISTS `lovd_genes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_genes` (
  `id` INT(10) AUTO_INCREMENT,
  `geneid` varchar(25) NOT NULL,
  `name` varchar(255) NOT NULL,
  `gene_pheno` varchar(255) DEFAULT NULL,
  `gene_ccds` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `chromosome` varchar(2) DEFAULT NULL,
  `chrom_band` varchar(40) NOT NULL DEFAULT '',
  `imprinting` varchar(10) NOT NULL DEFAULT 'unknown',
  `refseq_genomic` varchar(15) NOT NULL DEFAULT '',
  `refseq_UD` varchar(25) NOT NULL DEFAULT '',
  `reference` varchar(255) NOT NULL DEFAULT '',
  `url_homepage` varchar(255) NOT NULL DEFAULT '',
  `url_external` text,
  `allow_download` tinyint(1) NOT NULL DEFAULT '0',
  `id_hgnc` VARCHAR(255) DEFAULT NULL,
  `id_entrez` int(10) unsigned DEFAULT NULL,
  `id_omim` int(10) unsigned DEFAULT NULL,
  `show_hgmd` tinyint(1) NOT NULL DEFAULT '0',
  `show_genecards` tinyint(1) NOT NULL DEFAULT '0',
  `show_genetests` tinyint(1) NOT NULL DEFAULT '0',
  `note_index` text,
  `note_listing` text,
  `refseq` varchar(1) NOT NULL DEFAULT '',
  `refseq_url` varchar(255) NOT NULL DEFAULT '',
  `disclaimer` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `disclaimer_text` text,
  `header` text,
  `header_align` tinyint(1) NOT NULL DEFAULT '-1',
  `footer` text,
  `footer_align` tinyint(1) NOT NULL DEFAULT '-1',
  `created_by` smallint(5)  DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP, -- NOT NULL
  `edited_by` smallint(5)  DEFAULT NULL,
  `edited_date` datetime DEFAULT NULL,
  `updated_by` smallint(5)  DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_gene_key` (`geneid`, `gene_pheno`, `gene_ccds`, `source`, `chromosome`),
  INDEX `name` (`name`),
  INDEX `geneid` (`geneid`),
  KEY `id_hgnc` (`id_hgnc`),
  KEY `chromosome` (`chromosome`),
  KEY `created_by` (`created_by`),
  KEY `edited_by` (`edited_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `lovd_genes_fk_chromosome` FOREIGN KEY (`chromosome`) REFERENCES `lovd_chromosomes` (`name`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_genes_fk_created_by` FOREIGN KEY (`created_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_genes_fk_edited_by` FOREIGN KEY (`edited_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_genes_fk_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_genes`
--

LOCK TABLES `lovd_genes` WRITE;
/*!40000 ALTER TABLE `lovd_genes` DISABLE KEYS */;
/*!40000 ALTER TABLE `lovd_genes` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `lovd_gene_panels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_gene_panels` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `panel_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY (panel_name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


LOCK TABLES `lovd_gene_panels` WRITE;
/*!40000 ALTER TABLE `lovd_gene_panels` DISABLE KEYS */;
/*!40000 ALTER TABLE `lovd_gene_panels` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `lovd_panels2genes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_panels2genes` (
  `panelid` INT(10) NOT NULL,
  -- `genename` varchar(255) NOT NULL,
  `geneid` INT(10) NOT NULL,
  -- `geneid_hgnc` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`panelid`,`geneid`),
  FOREIGN KEY (`panelid`) REFERENCES `lovd_gene_panels` (`id`),
  FOREIGN KEY (`geneid`) REFERENCES `lovd_genes` (`id`)
  --   FOREIGN KEY (`geneid`,`genename`,`geneid_hgnc`) REFERENCES `lovd_genes` (`id`,`name`,`id_hgnc`) PRIMARY KEY (`panelid`,`genename`,`geneid`, `geneid_hgnc`),
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `lovd_panels2genes` WRITE;
/*!40000 ALTER TABLE `lovd_panels2genes` DISABLE KEYS */;
/*!40000 ALTER TABLE `lovd_panels2genes` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Table structure for table `lovd_genes2diseases`
--

DROP TABLE IF EXISTS `lovd_genes2diseases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_genes2diseases` (
  `geneid` INT(10) NOT NULL,
  `diseaseid` smallint(5)  NOT NULL,
  PRIMARY KEY (`geneid`,`diseaseid`),
  KEY `diseaseid` (`diseaseid`),
  CONSTRAINT `lovd_genes2diseases_fk_diseaseid` FOREIGN KEY (`diseaseid`) REFERENCES `lovd_diseases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lovd_genes2diseases_fk_geneid` FOREIGN KEY (`geneid`) REFERENCES `lovd_genes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_genes2diseases`
--

LOCK TABLES `lovd_genes2diseases` WRITE;
/*!40000 ALTER TABLE `lovd_genes2diseases` DISABLE KEYS */;
/*!40000 ALTER TABLE `lovd_genes2diseases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_individuals`
--

DROP TABLE IF EXISTS `lovd_individuals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_individuals` (
  `id` mediumint(8)  NOT NULL AUTO_INCREMENT,
  `fatherid` mediumint(8)  DEFAULT NULL,
  `motherid` mediumint(8)  DEFAULT NULL,
  `panelid` mediumint(8)  DEFAULT NULL,
  `panel_size` mediumint(8) unsigned NOT NULL DEFAULT '1',
  `owned_by` smallint(5)  DEFAULT NULL,
  `statusid` tinyint(1) unsigned DEFAULT NULL,
  `created_by` smallint(5)  DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `edited_by` smallint(5)  DEFAULT NULL,
  `edited_date` datetime DEFAULT NULL,
  `Individual/Lab_ID` varchar(50) DEFAULT NULL,
  `Individual/Reference` varchar(200) DEFAULT NULL,
  `Individual/Remarks` text,
  `Individual/Remarks_Non_Public` text,
  `hash` varchar(255) DEFAULT NULL,
  `s3_location` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fatherid` (`fatherid`),
  KEY `motherid` (`motherid`),
  KEY `owned_by` (`owned_by`),
  KEY `statusid` (`statusid`),
  KEY `created_by` (`created_by`),
  KEY `edited_by` (`edited_by`),
  KEY `lovd_individuals_fk_panelid` (`panelid`),
  INDEX `panelid` (`panelid`),
  INDEX `Individual/Reference` (`Individual/Reference`),
  UNIQUE KEY `hash_ref_num` (`Individual/Reference`,`hash`),
  CONSTRAINT `lovd_individuals_fk_created_by` FOREIGN KEY (`created_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_individuals_fk_edited_by` FOREIGN KEY (`edited_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_individuals_fk_fatherid` FOREIGN KEY (`fatherid`) REFERENCES `lovd_individuals` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_individuals_fk_motherid` FOREIGN KEY (`motherid`) REFERENCES `lovd_individuals` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_individuals_fk_owned_by` FOREIGN KEY (`owned_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_individuals_fk_panelid` FOREIGN KEY (`panelid`) REFERENCES `lovd_individuals` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_individuals_fk_statusid` FOREIGN KEY (`statusid`) REFERENCES `lovd_data_status` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8; -- AUTO INCREMENT WAS SET TO 25
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_individuals`
--

LOCK TABLES `lovd_individuals` WRITE;
/*!40000 ALTER TABLE `lovd_individuals` DISABLE KEYS */;
-- INSERT INTO `lovd_individuals` VALUES (00000001,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-25 16:07:09',NULL,NULL,NULL,'6969-69696-6969',NULL,NULL),(00000002,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-25 17:57:16',NULL,NULL,NULL,'2222-22222-2222',NULL,NULL),(00000003,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-25 17:57:49',NULL,NULL,NULL,'2222-22222-2222',NULL,NULL),(00000004,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-25 17:58:04',NULL,NULL,NULL,'2222-22222-2222',NULL,NULL),(00000005,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-25 20:27:57',NULL,NULL,NULL,'2222-22222-2222',NULL,NULL),(00000006,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-25 22:37:10',NULL,NULL,NULL,'2222-22222-2222',NULL,NULL),(00000007,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-25 22:37:27',NULL,NULL,NULL,'2222-22222-2222',NULL,NULL),(00000008,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-26 00:46:44',NULL,NULL,NULL,'2222-22222-2222',NULL,NULL),(00000009,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-26 01:10:57',NULL,NULL,NULL,'2222-22222-2222',NULL,NULL),(00000010,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-26 01:11:15',NULL,NULL,NULL,'2222-22222-2222',NULL,NULL),(00000011,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-26 02:07:28',NULL,NULL,NULL,'2222-22222-2222',NULL,NULL),(00000012,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-26 03:59:00',NULL,NULL,NULL,'2222-22222-2222',NULL,NULL),(00000013,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-26 03:59:20',NULL,NULL,NULL,'2222-22222-2222',NULL,NULL),(00000014,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-26 06:58:36',NULL,NULL,NULL,'2222-22222-2222',NULL,NULL),(00000015,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-26 07:09:23',NULL,NULL,NULL,'2222-22222-2222',NULL,NULL),(00000016,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-26 07:09:49',NULL,NULL,NULL,'2222-22222-2222',NULL,NULL),(00000017,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-26 08:22:36',NULL,NULL,NULL,'2222-22222-2222',NULL,NULL),(00000018,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-26 10:40:04',NULL,NULL,NULL,'2222-22222-2222',NULL,NULL),(00000019,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-26 10:40:29',NULL,NULL,NULL,'2222-22222-2222',NULL,NULL),(00000020,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-26 12:41:29',NULL,NULL,NULL,'3333-33333-3333',NULL,NULL),(00000021,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-26 13:14:51',NULL,NULL,NULL,'3333-33333-3333',NULL,NULL),(00000022,NULL,NULL,NULL,1,NULL,9,NULL,'2021-02-26 13:15:26',NULL,NULL,NULL,'3333-33333-3333',NULL,NULL),(00000023,NULL,NULL,NULL,1,NULL,9,NULL,'2021-03-01 08:00:18',NULL,NULL,NULL,'4444-44444-4444',NULL,NULL),(00000024,NULL,NULL,NULL,1,NULL,9,NULL,'2021-03-01 12:00:19',NULL,NULL,NULL,'5555-55555-5555',NULL,NULL);
/*!40000 ALTER TABLE `lovd_individuals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_individuals2diseases`
--

DROP TABLE IF EXISTS `lovd_individuals2diseases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_individuals2diseases` (
  `individualid` mediumint(8)  NOT NULL,
  `diseaseid` smallint(5)  NOT NULL,
  PRIMARY KEY (`individualid`,`diseaseid`),
  KEY `diseaseid` (`diseaseid`),
  CONSTRAINT `lovd_individuals2diseases_fk_diseaseid` FOREIGN KEY (`diseaseid`) REFERENCES `lovd_diseases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lovd_individuals2diseases_fk_individualid` FOREIGN KEY (`individualid`) REFERENCES `lovd_individuals` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_individuals2diseases`
--

LOCK TABLES `lovd_individuals2diseases` WRITE;
/*!40000 ALTER TABLE `lovd_individuals2diseases` DISABLE KEYS */;
/*!40000 ALTER TABLE `lovd_individuals2diseases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_links`
--

DROP TABLE IF EXISTS `lovd_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_links` (
  `id` tinyint(3)  NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `pattern_text` varchar(25) NOT NULL,
  `replace_text` text NOT NULL,
  `description` text,
  `created_by` smallint(5)  DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `edited_by` smallint(5)  DEFAULT NULL,
  `edited_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `pattern_text` (`pattern_text`),
  KEY `created_by` (`created_by`),
  KEY `edited_by` (`edited_by`),
  CONSTRAINT `lovd_links_fk_created_by` FOREIGN KEY (`created_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_links_fk_edited_by` FOREIGN KEY (`edited_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8; -- AUTO INCREMENT WAS SET TO 7
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_links`
--

LOCK TABLES `lovd_links` WRITE;
/*!40000 ALTER TABLE `lovd_links` DISABLE KEYS */;
INSERT INTO `lovd_links` VALUES (001,'PubMed','{PMID:[1]:[2]}','<A href=\"https://www.ncbi.nlm.nih.gov/pubmed/[2]\" target=\"_blank\">[1]</A>','Links to abstracts in the PubMed database.\r\n[1] = The name of the author(s), possibly followed by the year of publication.\r\n[2] = The PubMed ID.\r\n\r\nExample:\r\n{PMID:Fokkema et al. (2011):21520333}',00000,'2020-03-31 08:02:00',NULL,NULL),(002,'DbSNP','{dbSNP:[1]}','<A href=\"https://www.ncbi.nlm.nih.gov/SNP/snp_ref.cgi?rs=[1]\" target=\"_blank\">dbSNP</A>','Links to the DbSNP database.\r\n[1] = The DbSNP ID.\r\n\r\nExamples:\r\n{dbSNP:rs193143796}\r\n{dbSNP:193143796}',00000,'2020-03-31 08:02:00',NULL,NULL),(003,'GenBank','{GenBank:[1]}','<A href=\"https://www.ncbi.nlm.nih.gov/nuccore/[1]\" target=\"_blank\">GenBank</A>','Links to GenBank sequences.\r\n[1] = The GenBank ID.\r\n\r\nExamples:\r\n{GenBank:NG_012232.1}\r\n{GenBank:NC_000001.10}',00000,'2020-03-31 08:02:00',NULL,NULL),(004,'OMIM','{OMIM:[1]:[2]}','<A href=\"http://www.omim.org/entry/[1]#[2]\" target=\"_blank\">(OMIM [2])</A>','Links to an allelic variant on the gene\'s OMIM page.\r\n[1] = The OMIM gene ID.\r\n[2] = The number of the OMIM allelic variant on that page.\r\n\r\nExamples:\r\n{OMIM:300377:0021}\r\n{OMIM:188840:0003}',00000,'2020-03-31 08:02:00',NULL,NULL),(005,'DOI','{DOI:[1]:[2]}','<A href=\"http://dx.doi.org/[2]\" target=\"_blank\">[1]</A>','Links directly to an article using the DOI.\r\n[1] = The name of the author(s), possibly followed by the year of publication.\r\n[2] = The DOI.\r\n\r\nExample:\r\n{DOI:Fokkema et al. (2011):10.1002/humu.21438}',00000,'2020-03-31 08:02:00',NULL,NULL),(006,'Alamut','{Alamut:[1]:[2]}','<A href=\"http://127.0.0.1:10000/show?request=[1]:[2]\" target=\"_blank\">Alamut</A>','Links directly to the variant in the Alamut software.\r\n[1] = The chromosome letter or number.\r\n[2] = The genetic change on genome level.\r\n\r\nExample:\r\n{Alamut:16:21854780G>A}',00000,'2020-03-31 08:02:00',NULL,NULL);
/*!40000 ALTER TABLE `lovd_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_logs`
--

DROP TABLE IF EXISTS `lovd_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_logs` (
  `name` varchar(10) NOT NULL,
  `date` datetime NOT NULL,
  `mtime` mediumint(6)  NOT NULL,
  `userid` smallint(5)  DEFAULT NULL,
  `event` varchar(20) NOT NULL,
  `log` text NOT NULL,
  PRIMARY KEY (`name`,`date`,`mtime`),
  KEY `userid` (`userid`),
  CONSTRAINT `lovd_logs_fk_userid` FOREIGN KEY (`userid`) REFERENCES `lovd_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_logs`
--

LOCK TABLES `lovd_logs` WRITE;
/*!40000 ALTER TABLE `lovd_logs` DISABLE KEYS */;
INSERT INTO `lovd_logs` VALUES ('Install','2020-03-31 08:03:15',927765,NULL,'Installation','Installation of LOVD 3.0-23 complete');
/*!40000 ALTER TABLE `lovd_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_modules`
--

DROP TABLE IF EXISTS `lovd_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_modules` (
  `id` varchar(15) NOT NULL,
  `name` varchar(50) NOT NULL,
  `version` varchar(15) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `settings` text,
  `installed_date` date NOT NULL,
  `updated_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_modules`
--

LOCK TABLES `lovd_modules` WRITE;
/*!40000 ALTER TABLE `lovd_modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `lovd_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_phenotypes`
--

DROP TABLE IF EXISTS `lovd_phenotypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_phenotypes` (
  `id` int(10)  NOT NULL AUTO_INCREMENT,
  `diseaseid` smallint(5)  NOT NULL,
  `individualid` mediumint(8)  NOT NULL,
  `owned_by` smallint(5)  DEFAULT NULL,
  `statusid` tinyint(1) unsigned DEFAULT NULL,
  `created_by` smallint(5)  DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `edited_by` smallint(5)  DEFAULT NULL,
  `edited_date` datetime DEFAULT NULL,
  `Phenotype/Additional` text,
  `Phenotype/Inheritance` varchar(50) DEFAULT NULL,
  `Phenotype/Age` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `diseaseid` (`diseaseid`),
  KEY `individualid` (`individualid`),
  KEY `owned_by` (`owned_by`),
  KEY `statusid` (`statusid`),
  KEY `created_by` (`created_by`),
  KEY `edited_by` (`edited_by`),
  CONSTRAINT `lovd_phenotypes_fk_created_by` FOREIGN KEY (`created_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_phenotypes_fk_diseaseid` FOREIGN KEY (`diseaseid`) REFERENCES `lovd_diseases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lovd_phenotypes_fk_edited_by` FOREIGN KEY (`edited_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_phenotypes_fk_individualid` FOREIGN KEY (`individualid`) REFERENCES `lovd_individuals` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lovd_phenotypes_fk_owned_by` FOREIGN KEY (`owned_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_phenotypes_fk_statusid` FOREIGN KEY (`statusid`) REFERENCES `lovd_data_status` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_phenotypes`
--

LOCK TABLES `lovd_phenotypes` WRITE;
/*!40000 ALTER TABLE `lovd_phenotypes` DISABLE KEYS */;
/*!40000 ALTER TABLE `lovd_phenotypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_scheduled_imports`
--

DROP TABLE IF EXISTS `lovd_scheduled_imports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_scheduled_imports` (
  `filename` varchar(255) NOT NULL,
  `priority` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `in_progress` tinyint(1) NOT NULL DEFAULT '0',
  `scheduled_by` smallint(5)  DEFAULT NULL,
  `scheduled_date` datetime NOT NULL,
  `process_errors` text,
  `processed_by` smallint(5)  DEFAULT NULL,
  `processed_date` datetime DEFAULT NULL,
  PRIMARY KEY (`filename`),
  KEY `scheduled_by` (`scheduled_by`),
  KEY `processed_by` (`processed_by`),
  CONSTRAINT `lovd_scheduled_imports_fk_processed_by` FOREIGN KEY (`processed_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_scheduled_imports_fk_scheduled_by` FOREIGN KEY (`scheduled_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_scheduled_imports`
--

LOCK TABLES `lovd_scheduled_imports` WRITE;
/*!40000 ALTER TABLE `lovd_scheduled_imports` DISABLE KEYS */;
/*!40000 ALTER TABLE `lovd_scheduled_imports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_screenings`
--

DROP TABLE IF EXISTS `lovd_screenings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_screenings` (
  `id` int(10)  NOT NULL AUTO_INCREMENT,
  `individualid` mediumint(8)  NOT NULL,
  `variants_found` tinyint(1) NOT NULL DEFAULT '1',
  `Sequencing/Machine` varchar(50) NOT NULL,
  `analysis` varchar(50),
  `owned_by` smallint(5)  DEFAULT NULL,
  `created_by` smallint(5)  DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `edited_by` smallint(5)  DEFAULT NULL,
  `edited_date` datetime DEFAULT NULL,
  `Screening/Technique` text,
  `Screening/Template` text,
  `Annotated` varchar(3) DEFAULT NULL,
  `Vep_databases` TEXT DEFAULT NULL,
  `Vep_version` VARCHAR(10) DEFAULT NULL,
  `clinvar_version` DATE DEFAULT NULL,
  `dbSNP_custom_version` VARCHAR(100) DEFAULT NULL,
  `Filename` VARCHAR(100) DEFAULT NULL,
  `file_headers` MEDIUMTEXT DEFAULT NULL,
  `annotated_file_headers` MEDIUMTEXT DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  -- KEY `individualid` (`individualid`),
  KEY `owned_by` (`owned_by`),
  KEY `created_by` (`created_by`),
  KEY `edited_by` (`edited_by`),
  INDEX `individualid` (`individualid`),
  INDEX `Sequencing/Machine` (`Sequencing/Machine`),
  INDEX `analysis` (`analysis`),
  INDEX `Annotated` (`Annotated`),
  INDEX `Vep_version` (`Vep_version`),
  INDEX `Filename` (`Filename`),
  CONSTRAINT `lovd_screenings_fk_created_by` FOREIGN KEY (`created_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_screenings_fk_edited_by` FOREIGN KEY (`edited_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_screenings_fk_individualid` FOREIGN KEY (`individualid`) REFERENCES `lovd_individuals` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lovd_screenings_fk_owned_by` FOREIGN KEY (`owned_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8; -- AUTO INCREMENT WAS SET TO 25
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_screenings`
--

LOCK TABLES `lovd_screenings` WRITE;
/*!40000 ALTER TABLE `lovd_screenings` DISABLE KEYS */;
-- INSERT INTO `lovd_screenings` VALUES (0000000001,00000001,1,NULL,NULL,'2021-02-25 16:07:09',NULL,NULL,NULL,NULL),(0000000002,00000002,1,NULL,NULL,'2021-02-25 17:57:16',NULL,NULL,NULL,NULL),(0000000003,00000003,1,NULL,NULL,'2021-02-25 17:57:49',NULL,NULL,NULL,NULL),(0000000004,00000004,1,NULL,NULL,'2021-02-25 17:58:04',NULL,NULL,NULL,NULL),(0000000005,00000005,1,NULL,NULL,'2021-02-25 20:27:57',NULL,NULL,NULL,NULL),(0000000006,00000006,1,NULL,NULL,'2021-02-25 22:37:10',NULL,NULL,NULL,NULL),(0000000007,00000007,1,NULL,NULL,'2021-02-25 22:37:27',NULL,NULL,NULL,NULL),(0000000008,00000008,1,NULL,NULL,'2021-02-26 00:46:44',NULL,NULL,NULL,NULL),(0000000009,00000009,1,NULL,NULL,'2021-02-26 01:10:57',NULL,NULL,NULL,NULL),(0000000010,00000010,1,NULL,NULL,'2021-02-26 01:11:15',NULL,NULL,NULL,NULL),(0000000011,00000011,1,NULL,NULL,'2021-02-26 02:07:28',NULL,NULL,NULL,NULL),(0000000012,00000012,1,NULL,NULL,'2021-02-26 03:59:00',NULL,NULL,NULL,NULL),(0000000013,00000013,1,NULL,NULL,'2021-02-26 03:59:20',NULL,NULL,NULL,NULL),(0000000014,00000014,1,NULL,NULL,'2021-02-26 06:58:37',NULL,NULL,NULL,NULL),(0000000015,00000015,1,NULL,NULL,'2021-02-26 07:09:23',NULL,NULL,NULL,NULL),(0000000016,00000016,1,NULL,NULL,'2021-02-26 07:09:49',NULL,NULL,NULL,NULL),(0000000017,00000017,1,NULL,NULL,'2021-02-26 08:22:36',NULL,NULL,NULL,NULL),(0000000018,00000018,1,NULL,NULL,'2021-02-26 10:40:04',NULL,NULL,NULL,NULL),(0000000019,00000019,1,NULL,NULL,'2021-02-26 10:40:29',NULL,NULL,NULL,NULL),(0000000020,00000020,1,NULL,NULL,'2021-02-26 12:41:29',NULL,NULL,NULL,NULL),(0000000021,00000021,1,NULL,NULL,'2021-02-26 13:14:51',NULL,NULL,NULL,NULL),(0000000022,00000022,1,NULL,NULL,'2021-02-26 13:15:26',NULL,NULL,NULL,NULL),(0000000023,00000023,1,NULL,NULL,'2021-03-01 08:00:18',NULL,NULL,NULL,NULL),(0000000024,00000024,1,NULL,NULL,'2021-03-01 12:00:19',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `lovd_screenings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_screenings2genes`
--

DROP TABLE IF EXISTS `lovd_screenings2genes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_screenings2genes` (
  `screeningid` int(10)  NOT NULL,
  `geneid` INT(10) NOT NULL,
  PRIMARY KEY (`screeningid`,`geneid`),
  KEY `screeningid` (`screeningid`),
  KEY `geneid` (`geneid`),
  CONSTRAINT `lovd_screenings2genes_fk_geneid` FOREIGN KEY (`geneid`) REFERENCES `lovd_genes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lovd_screenings2genes_fk_screeningid` FOREIGN KEY (`screeningid`) REFERENCES `lovd_screenings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_screenings2genes`
--

LOCK TABLES `lovd_screenings2genes` WRITE;
/*!40000 ALTER TABLE `lovd_screenings2genes` DISABLE KEYS */;
/*!40000 ALTER TABLE `lovd_screenings2genes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_screenings2variants`
--

DROP TABLE IF EXISTS `lovd_screenings2variants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_screenings2variants` (
  `screeningid` int(10)  NOT NULL,
  `variantid` int(10)  NOT NULL,
  PRIMARY KEY (`screeningid`,`variantid`),
  KEY `screeningid` (`screeningid`),
  KEY `variantid` (`variantid`),
  CONSTRAINT `lovd_screenings2variants_fk_screeningid` FOREIGN KEY (`screeningid`) REFERENCES `lovd_screenings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lovd_screenings2variants_fk_variantid` FOREIGN KEY (`variantid`) REFERENCES `lovd_variants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_screenings2variants`
--


--
-- Table structure for table `lovd_shared_columns`
--

DROP TABLE IF EXISTS `lovd_shared_columns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_shared_columns` (
  `geneid` INT(10) NOT NULL,
  `diseaseid` smallint(5)  DEFAULT NULL,
  `colid` varchar(100) NOT NULL,
  `col_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `width` smallint(5) unsigned NOT NULL DEFAULT '50',
  `mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `description_form` text,
  `description_legend_short` text,
  `description_legend_full` text,
  `select_options` text,
  `public_view` tinyint(1) NOT NULL DEFAULT '1',
  `public_add` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` smallint(5)  DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `edited_by` smallint(5)  DEFAULT NULL,
  `edited_date` datetime DEFAULT NULL,
  UNIQUE KEY `geneid` (`geneid`,`colid`),
  UNIQUE KEY `diseaseid` (`diseaseid`,`colid`),
  KEY `colid` (`colid`),
  KEY `created_by` (`created_by`),
  KEY `edited_by` (`edited_by`),
  CONSTRAINT `lovd_shared_columns_fk_colid` FOREIGN KEY (`colid`) REFERENCES `lovd_active_columns` (`colid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lovd_shared_columns_fk_created_by` FOREIGN KEY (`created_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_shared_columns_fk_diseaseid` FOREIGN KEY (`diseaseid`) REFERENCES `lovd_diseases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lovd_shared_columns_fk_edited_by` FOREIGN KEY (`edited_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_shared_columns_fk_geneid` FOREIGN KEY (`geneid`) REFERENCES `lovd_genes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_shared_columns`
--

LOCK TABLES `lovd_shared_columns` WRITE;
/*!40000 ALTER TABLE `lovd_shared_columns` DISABLE KEYS */;
-- INSERT INTO `lovd_shared_columns` VALUES (NULL,00000,'Phenotype/Age',0,100,0,'Type 35y for 35 years, 04y08m for 4 years and 8 months, 18y? for around 18 years, >54y for older than 54, ? for unknown.','The age at which the individual was examined, if known. 04y08m = 4 years and 8 months.','The age at which the individual was examined, if known.\r\n<UL style=\"margin-top:0px;\">\r\n  <LI>35y = 35 years</LI>\r\n  <LI>04y08m = 4 years and 8 months</LI>\r\n  <LI>18y? = around 18 years</LI>\r\n  <LI>&gt;54y = older than 54</LI>\r\n  <LI>? = unknown</LI>\r\n</UL>','',1,1,00000,'2020-03-31 08:02:00',NULL,NULL);
/*!40000 ALTER TABLE `lovd_shared_columns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_status`
--

DROP TABLE IF EXISTS `lovd_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_status` (
  `lock_update` tinyint(1) NOT NULL DEFAULT '0',
  `version` varchar(15) NOT NULL,
  `signature` char(32) NOT NULL,
  `update_checked_date` datetime DEFAULT NULL,
  `update_version` varchar(15) DEFAULT NULL,
  `update_level` tinyint(1) unsigned DEFAULT NULL,
  `update_description` text,
  `update_released_date` date DEFAULT NULL,
  `installed_date` date NOT NULL,
  `updated_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_status`
--

LOCK TABLES `lovd_status` WRITE;
/*!40000 ALTER TABLE `lovd_status` DISABLE KEYS */;
INSERT INTO `lovd_status` VALUES (0,'3.0-23','6f0068a4c4bcb19e1636fca5e4b0b1eb','2021-02-25 15:12:41','3.0-26',7,'3.0-26 : Added an \"LOVD light\" switch for large databases; Fixed query error when creating or editing an entry and leaving a decimal field empty; Fixed bug; HGNC IDs weren\'t handled well in the submission API; Fixed the link from the Diseases detailed view to HPO; Fixed reCAPTCHA for users in China; Fixed problems with MySQL 8.0.x.\n              3.0-25 : Added the possibility to download entire submissions; Added feature to merge entries (Individuals, Screenings); Improved the submission API to allow variant-only submissions and to directly process submissions if configured to do so; Extended search capabilities by allowing searching for prefix or suffix; Added a link to MobiDetails, an annotation platform dedicated to the interpretation of DNA variants; Fixed link to the Ensembl and NCBI genome browsers.\n              3.0-24 : Added new features to allow Curators and up to more quickly curate new data; APIs and views now sort transcripts based on the number of variants they contain; Created a variant verification script using the Variant Validator service; Improved the \"forgot my password\" feature; The entered IP value to restrict an account was not validated when registering a new account; The JSON variant API wasn\'t selecting data properly; The registration spam filter rejected too many real registrations.\n','2021-02-22','2020-03-31',NULL);
/*!40000 ALTER TABLE `lovd_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_transcripts`
--

DROP TABLE IF EXISTS `lovd_transcripts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_transcripts` (
  `id` mediumint(8)  NOT NULL AUTO_INCREMENT,
  `geneid` INT(10) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `id_mutalyzer` tinyint(3)  DEFAULT NULL,
  `id_ncbi` varchar(255) NOT NULL DEFAULT '',
  `id_ensembl` varchar(255) NOT NULL DEFAULT '',
  `id_protein_ncbi` varchar(255) NOT NULL DEFAULT '',
  `id_protein_ensembl` varchar(255) NOT NULL DEFAULT '',
  `id_protein_uniprot` varchar(8) NOT NULL DEFAULT '',
  `remarks` text,
  `position_c_mrna_start` smallint(5) , 
  `position_c_mrna_end` mediumint(8) unsigned , 
  `position_c_cds_end` mediumint(8) unsigned , 
  `position_g_mrna_start` int(10) unsigned , 
  `position_g_mrna_end` int(10) unsigned , 
  `created_by` smallint(5)  DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `edited_by` smallint(5)  DEFAULT NULL,
  `edited_date` datetime DEFAULT NULL,
  `Allele` VARCHAR(255) DEFAULT NULL,
  `Consequence` VARCHAR(255) DEFAULT NULL,
  `IMPACT` VARCHAR(255) DEFAULT NULL,
  `Gene` VARCHAR(255) DEFAULT NULL,
  `Feature_type` VARCHAR(255) DEFAULT NULL,
  `Feature` VARCHAR(255) DEFAULT NULL,
  `BIOTYPE` VARCHAR(255) DEFAULT NULL,
  `EXON` VARCHAR(255) DEFAULT NULL,
  `INTRON` VARCHAR(255) DEFAULT NULL,
  `HGVSc` text DEFAULT NULL,
  `HGVSp` text DEFAULT NULL,
  `cDNA_position` VARCHAR(255) DEFAULT NULL,
  `CDS_position` VARCHAR(255) DEFAULT NULL,
  `Protein_position` VARCHAR(255) DEFAULT NULL,
  `Amino_acids` VARCHAR(255) DEFAULT NULL,
  `Codons` VARCHAR(255) DEFAULT NULL,
  `Existing_variation` VARCHAR(255),
  `DISTANCE` VARCHAR(255),
  `STRAND` VARCHAR(255),
  `FLAGS` VARCHAR(255),
  `VARIANT_CLASS` VARCHAR(255),
  `CANONICAL` VARCHAR(255),
  `MANE` VARCHAR(255),
  `MANE_SELECT` VARCHAR(255),
  `MANE_PLUS` VARCHAR(255),
  `TSL` VARCHAR(255),
  `APPRIS` VARCHAR(255),
  `ENSP` VARCHAR(255),
  `SWISSPROT` VARCHAR(255),
  `TREMBL` text,
  `UNIPARC` VARCHAR(255),
  `UNIPROT_ISOFORM` VARCHAR(255),
  `REFSEQ_MATCH` VARCHAR(255),
  `SOURCE` VARCHAR(255),
  `REFSEQ_OFFSET` VARCHAR(255),
  `GENE_PHENO` VARCHAR(255),
  `SIFT` VARCHAR(255),
  `PolyPhen` VARCHAR(255),
  `DOMAINS` text,
  `miRNA` VARCHAR(255),
  `HGVS_OFFSET` VARCHAR(255),
  `AF` DOUBLE DEFAULT NULL,
  `AFR_AF` DOUBLE DEFAULT NULL,
  `AMR_AF` DOUBLE DEFAULT NULL,
  `EAS_AF` DOUBLE DEFAULT NULL,
  `EUR_AF` DOUBLE DEFAULT NULL,
  `SAS_AF` DOUBLE DEFAULT NULL,
  `AA_AF` DOUBLE DEFAULT NULL,
  `EA_AF` DOUBLE DEFAULT NULL,
  `gnomAD_AF` DOUBLE,
  `gnomAD_AFR_AF` DOUBLE,
  `gnomAD_AMR_AF` DOUBLE,
  `gnomAD_ASJ_AF` DOUBLE,
  `gnomAD_EAS_AF` DOUBLE,
  `gnomAD_FIN_AF` DOUBLE,
  `gnomAD_NFE_AF` DOUBLE,
  `gnomAD_OTH_AF` DOUBLE,
  `gnomAD_SAS_AF` DOUBLE,
  `MAX_AF` VARCHAR(255),
  `MAX_AF_POPS` text,
  `CLIN_SIG` VARCHAR(255),
  `SOMATIC` VARCHAR(255),
  `PHENO` VARCHAR(255),
  `PUBMED` MEDIUMTEXT,
  `MOTIF_NAME` text,
  `MOTIF_POS` text,
  `HIGH_INF_POS` text,
  `MOTIF_SCORE_CHANGE` text,
  `TRANSCRIPTION_FACTORS` text,
  `LOVD` VARCHAR(255),
  `dbSNP_154_testing_version` VARCHAR(200),
  `dbSNP_custom_version` VARCHAR(200),
  `dbSNP_custom` VARCHAR(200),
  `artifact` boolean DEFAULT 0,
  `other` boolean DEFAULT 0,
  `other_text` MEDIUMTEXT DEFAULT '',
  `hash` varchar(255) DEFAULT NULL,
  `clinvarid` INT(10),
  `VariantOnGenome/DNA` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_ncbi` (`id_ncbi`), 
  INDEX `geneid` (`geneid`),
  KEY `created_by` (`created_by`),
  KEY `edited_by` (`edited_by`),
  INDEX `gnomAD_AF` (`gnomAD_AF`),
  INDEX `gnomAD_AFR_AF` (`gnomAD_AFR_AF`),
  INDEX `gnomAD_AMR_AF` (`gnomAD_AMR_AF`),
  INDEX `gnomAD_ASJ_AF` (`gnomAD_ASJ_AF`),
  INDEX `gnomAD_EAS_AF` (`gnomAD_EAS_AF`),
  INDEX `gnomAD_FIN_AF` (`gnomAD_FIN_AF`),
  INDEX `gnomAD_NFE_AF` (`gnomAD_NFE_AF`),
  INDEX `gnomAD_OTH_AF` (`gnomAD_OTH_AF`),
  INDEX `gnomAD_SAS_AF` (`gnomAD_SAS_AF`),
  INDEX `Feature` (`Feature`),
  INDEX `BIOTYPE` (`BIOTYPE`),
  INDEX `Consequence` (`Consequence`),
  CONSTRAINT `lovd_transcripts_fk_created_by` FOREIGN KEY (`created_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_transcripts_fk_edited_by` FOREIGN KEY (`edited_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_transcripts_fk_geneid` FOREIGN KEY (`geneid`) REFERENCES `lovd_genes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_transcripts`
--

LOCK TABLES `lovd_transcripts` WRITE;
/*!40000 ALTER TABLE `lovd_transcripts` DISABLE KEYS */;
/*!40000 ALTER TABLE `lovd_transcripts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_users`
--

DROP TABLE IF EXISTS `lovd_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_users` (
  `id` smallint(5)  NOT NULL AUTO_INCREMENT,
  `orcid_id` char(19) DEFAULT NULL,
  `orcid_confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(75) NOT NULL,
  `institute` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL DEFAULT '',
  `telephone` varchar(50) NOT NULL DEFAULT '',
  `address` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `countryid` char(2) DEFAULT NULL,
  `email` text NOT NULL,
  `email_confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `password` char(50) NOT NULL,
  `password_autogen` char(50) DEFAULT NULL,
  `password_force_change` tinyint(1) NOT NULL DEFAULT '0',
  `auth_token` char(32) DEFAULT NULL,
  `auth_token_expires` datetime DEFAULT NULL,
  `phpsessid` char(32) DEFAULT NULL,
  `saved_work` text,
  `level` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `allowed_ip` varchar(255) NOT NULL DEFAULT '*',
  `login_attempts` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `created_by` smallint(5)  DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `edited_by` smallint(5)  DEFAULT NULL,
  `edited_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `orcid_id` (`orcid_id`),
  KEY `countryid` (`countryid`),
  KEY `created_by` (`created_by`),
  KEY `edited_by` (`edited_by`),
  CONSTRAINT `lovd_users_fk_countryid` FOREIGN KEY (`countryid`) REFERENCES `lovd_countries` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_users_fk_created_by` FOREIGN KEY (`created_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_users_fk_edited_by` FOREIGN KEY (`edited_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8; -- AUTO INCREMENT WAS SET TO 2
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_users`
--

LOCK TABLES `lovd_users` WRITE;
/*!40000 ALTER TABLE `lovd_users` DISABLE KEYS */;
INSERT INTO `lovd_users` VALUES (00000,NULL,0,'LOVD','','','','','',NULL,'',0,'','',NULL,0,NULL,NULL,NULL,NULL,0,'',9,NULL,00000,'2020-03-31 08:01:52',NULL,NULL),(00001,NULL,0,'Giannos Chatziagapis','NCSR Demokritos','','','NCSR Demokritos','Athens','GR','giannos.chatziagapis@gmail.com',0,'admin','','',0,NULL,NULL,'5d5930b02042539ce01eee990daba593','',9,'*',0,'2020-03-31 08:01:52',00001,'2020-03-31 08:01:52',NULL,NULL);
/*!40000 ALTER TABLE `lovd_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_users2genes`
--

DROP TABLE IF EXISTS `lovd_users2genes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_users2genes` (
  `userid` smallint(5)  NOT NULL,
  `geneid` INT(10) NOT NULL,
  `allow_edit` tinyint(1) NOT NULL DEFAULT '0',
  `show_order` tinyint(2) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`userid`,`geneid`),
  KEY `geneid` (`geneid`),
  CONSTRAINT `lovd_users2genes_fk_geneid` FOREIGN KEY (`geneid`) REFERENCES `lovd_genes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lovd_users2genes_fk_userid` FOREIGN KEY (`userid`) REFERENCES `lovd_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_users2genes`
--

LOCK TABLES `lovd_users2genes` WRITE;
/*!40000 ALTER TABLE `lovd_users2genes` DISABLE KEYS */;
/*!40000 ALTER TABLE `lovd_users2genes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_variant_effect`
--

DROP TABLE IF EXISTS `lovd_variant_effect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_variant_effect` (
  `id` tinyint(2)  NOT NULL,
  `name` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_variant_effect`
--

LOCK TABLES `lovd_variant_effect` WRITE;
/*!40000 ALTER TABLE `lovd_variant_effect` DISABLE KEYS */;
INSERT INTO `lovd_variant_effect` VALUES (00,'./.'),(01,'./-'),(03,'./-?'),(05,'./?'),(06,'./#'),(07,'./+?'),(08,'./+*'),(09,'./+'),(10,'-/.'),(11,'-/-'),(13,'-/-?'),(15,'-/?'),(16,'-/#'),(17,'-/+?'),(18,'-/+*'),(19,'-/+'),(30,'-?/.'),(31,'-?/-'),(33,'-?/-?'),(35,'-?/?'),(36,'-?/#'),(37,'-?/+?'),(38,'-?/+*'),(39,'-?/+'),(50,'?/.'),(51,'?/-'),(53,'?/-?'),(55,'?/?'),(56,'?/#'),(57,'?/+?'),(58,'?/+*'),(59,'?/+'),(60,'#/.'),(61,'#/-'),(63,'#/-?'),(65,'#/?'),(66,'#/#'),(67,'#/+?'),(68,'#/+*'),(69,'#/+'),(70,'+?/.'),(71,'+?/-'),(73,'+?/-?'),(75,'+?/?'),(76,'+?/#'),(77,'+?/+?'),(78,'+?/+*'),(79,'+?/+'),(80,'+*/.'),(81,'+*/-'),(83,'+*/-?'),(85,'+*/?'),(86,'+*/#'),(87,'+*/+?'),(88,'+*/+*'),(89,'+*/+'),(90,'+/.'),(91,'+/-'),(93,'+/-?'),(95,'+/?'),(96,'+/#'),(97,'+/+?'),(98,'+/+*'),(99,'+/+');
/*!40000 ALTER TABLE `lovd_variant_effect` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_variants`
--

DROP TABLE IF EXISTS `lovd_variants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_variants` (
  `id` int(10)  NOT NULL AUTO_INCREMENT,
  `allele` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `Variant_ID` varchar(255) DEFAULT NULL,
  `qual` varchar(255) DEFAULT NULL,
  `filter` varchar(255) DEFAULT NULL,
  `ref` varchar(255) NOT NULL,
  `alt` varchar(255) NOT NULL,
  `pos` varchar(255) NOT NULL,
  `mbq` varchar(100),
  `effectid` tinyint(2)  DEFAULT NULL,
  `chromosome` varchar(2) DEFAULT NULL,
  `position_g_start` int(10) unsigned DEFAULT NULL,
  `position_g_end` int(10) unsigned DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `mapping_flags` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `average_frequency` float unsigned DEFAULT NULL,
  `owned_by` smallint(5)  DEFAULT NULL,
  `statusid` tinyint(1) unsigned DEFAULT NULL,
  `created_by` smallint(5)  DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `edited_by` smallint(5)  DEFAULT NULL,
  `edited_date` datetime DEFAULT NULL,
  `VariantOnGenome/DBID` varchar(50) DEFAULT NULL,
  `VariantOnGenome/DNA` varchar(255) DEFAULT NULL,
  `VariantOnGenome/Reference` varchar(255) DEFAULT NULL,
  `AF` DOUBLE DEFAULT NULL,
  `TDP` int(10) DEFAULT NULL,
  `FDP` int(10) DEFAULT NULL,
  `STB` DOUBLE DEFAULT NULL,
  `ALTSTB` DOUBLE DEFAULT NULL,
  `REFSTB` DOUBLE DEFAULT NULL,
  `FORWARDSTA` DOUBLE DEFAULT NULL,
  `REVERSESTA` DOUBLE DEFAULT NULL,
  `NOSTA` DOUBLE DEFAULT NULL,
  `INFO_FIELDS` MEDIUMTEXT DEFAULT NULL,
  `FORMAT_FIELDS` MEDIUMTEXT DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `allele` (`allele`),
  KEY `effectid` (`effectid`),
  KEY `chromosome` (`chromosome`,`position_g_start`,`position_g_end`),
  KEY `average_frequency` (`average_frequency`),
  KEY `owned_by` (`owned_by`),
  KEY `statusid` (`statusid`),
  KEY `created_by` (`created_by`),
  KEY `edited_by` (`edited_by`),
  INDEX `AF` (`AF`),
  INDEX `TDP` (`TDP`),
  INDEX `FDP` (`FDP`),
  INDEX `STB` (`STB`),
  INDEX `ALTSTB` (`ALTSTB`),
  INDEX `REFSTB` (`REFSTB`),
  INDEX `FORWARDSTA` (`FORWARDSTA`),
  INDEX `REVERSESTA` (`REVERSESTA`),
  INDEX `NOSTA` (`NOSTA`),
  INDEX `VariantOnGenome/DNA` (`VariantOnGenome/DNA`),
  -- CONSTRAINT `lovd_variants_fk_allele` FOREIGN KEY (`allele`) REFERENCES `lovd_alleles` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `lovd_variants_fk_chromosome` FOREIGN KEY (`chromosome`) REFERENCES `lovd_chromosomes` (`name`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_variants_fk_created_by` FOREIGN KEY (`created_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_variants_fk_edited_by` FOREIGN KEY (`edited_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_variants_fk_effectid` FOREIGN KEY (`effectid`) REFERENCES `lovd_variant_effect` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_variants_fk_owned_by` FOREIGN KEY (`owned_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_variants_fk_statusid` FOREIGN KEY (`statusid`) REFERENCES `lovd_data_status` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8; -- AUTO INCREMENT WAS SET TO 158570
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Dumping data for table `lovd_variants`
--

LOCK TABLES `lovd_variants` WRITE;
/*!40000 ALTER TABLE `lovd_variants` DISABLE KEYS */;
/*!40000 ALTER TABLE `lovd_variants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lovd_variants_on_transcripts`
--

DROP TABLE IF EXISTS `lovd_variants_on_transcripts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_variants_on_transcripts` (
  `id` int(10)  NOT NULL,
  `transcriptid` mediumint(8)  NOT NULL,
  `effectid` tinyint(2)  DEFAULT NULL,
  `position_c_start` mediumint(9) DEFAULT NULL,
  `position_c_start_intron` int(11) DEFAULT NULL,
  `position_c_end` mediumint(9) DEFAULT NULL,
  `position_c_end_intron` int(11) DEFAULT NULL,
  `VariantOnTranscript/DNA` varchar(255) DEFAULT NULL,
  `VariantOnTranscript/Exon` varchar(7) DEFAULT NULL,
  `VariantOnTranscript/Protein` varchar(255) DEFAULT NULL,
  `VariantOnTranscript/RNA` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`,`transcriptid`),
  INDEX `transcriptid` (`transcriptid`),
  KEY `effectid` (`effectid`),
  KEY `position_c_start` (`position_c_start`,`position_c_end`),
  KEY `position_c_start_2` (`position_c_start`,`position_c_start_intron`,`position_c_end`,`position_c_end_intron`),
  CONSTRAINT `lovd_variants_on_transcripts_fk_effectid` FOREIGN KEY (`effectid`) REFERENCES `lovd_variant_effect` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `lovd_variants_on_transcripts_fk_id` FOREIGN KEY (`id`) REFERENCES `lovd_variants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lovd_variants_on_transcripts_fk_transcriptid` FOREIGN KEY (`transcriptid`) REFERENCES `lovd_transcripts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lovd_variants_on_transcripts`
--

LOCK TABLES `lovd_variants_on_transcripts` WRITE;
/*!40000 ALTER TABLE `lovd_variants_on_transcripts` DISABLE KEYS */;
/*!40000 ALTER TABLE `lovd_variants_on_transcripts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-01 13:26:19
DROP TABLE IF EXISTS `lovd_variants2transcripts`;

CREATE TABLE lovd_variants2transcripts (
`variantid` INT(10) ,
`transcriptid` MEDIUMINT(10) ,
`screeningid` int(10)  NOT NULL,
PRIMARY KEY (variantid, transcriptid),
INDEX `variantid` (`variantid`),
INDEX `transcriptid` (`transcriptid`),
INDEX `screeningid` (`screeningid`),
FOREIGN KEY (`transcriptid`) REFERENCES `lovd_transcripts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (`variantid`) REFERENCES `lovd_variants`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (`screeningid`) REFERENCES `lovd_screenings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `lovd_clinvar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_clinvar` (
  `id` INT(10) NOT NULL,
  `ClinVar_CLNSIG` varchar(200) NOT NULL,
  `ClinVar_CLNDN` varchar(200),
  `ClinVar_CLNREVSTAT` varchar(200),
  `version` DATE ,
  `vep` BOOLEAN DEFAULT 1,
  `hash` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`, `version`),
  KEY `id` (`id`),
  INDEX `ClinVar_CLNSIG` (`ClinVar_CLNSIG`),
  INDEX `ClinVar_CLNDN` (`ClinVar_CLNDN`),
  INDEX `ClinVar_CLNREVSTAT` (`ClinVar_CLNREVSTAT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS=1;

DROP TABLE IF EXISTS `lovd_transcripts2clinvar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_transcripts2clinvar` (
  `transcriptid` MEDIUMINT(10)  NOT NULL,
  `clinvarid` INT(10) NOT NULL,
  `date_added` DATE ,
  `clinvar_version` DATE ,
  `screeningid` int(10)  NOT NULL,
  INDEX `screeningid` (`screeningid`),
  PRIMARY KEY (`transcriptid`, `clinvarid`),
  FOREIGN KEY (`transcriptid`) REFERENCES `lovd_transcripts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`clinvarid`, `clinvar_version`) REFERENCES `lovd_clinvar` (`id`, `version`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`screeningid`) REFERENCES `lovd_screenings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- SET FOREIGN_KEY_CHECKS=1;

SET FOREIGN_KEY_CHECKS=0;


DROP TABLE IF EXISTS `lovd_form_variants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_form_variants` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `transcriptid` mediumint(8)  DEFAULT NULL,
  `transcript_variation` VARCHAR(400)  NOT NULL,
  `characterization` VARCHAR(400)  DEFAULT NULL,
  `clinician_characterization` VARCHAR(255)  NOT NULL,
  `zygote` VARCHAR(255)  DEFAULT NULL,
  `AF` DOUBLE DEFAULT NULL,
  `TDP` int(10) DEFAULT NULL,
  `FDP` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `transcriptid` (`transcriptid`),
  INDEX `transcript_variation` (`transcript_variation`),
  INDEX `characterization` (`characterization`),
  INDEX `clinician_characterization` (`clinician_characterization`),
  INDEX `AF` (`AF`),
  INDEX `TDP` (`TDP`),
  INDEX `FDP` (`FDP`),
  CONSTRAINT `lovd_form_variants_fk_transcriptid` FOREIGN KEY (`transcriptid`) REFERENCES `lovd_transcripts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `lovd_screenings2forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_screenings2forms` (
  `screeningid` INT(10) NOT NULL,
  `formid` INT(10) NOT NULL,
  `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (`screeningid`, `formid`),

  KEY `created_date` (`created_date`),

  CONSTRAINT `lovd_lovd_forms2variants_fk_screeningid` FOREIGN KEY (`screeningid`) REFERENCES `lovd_screenings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lovd_lovd_forms2variants_fk_formid` FOREIGN KEY (`formid`) REFERENCES `lovd_forms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lovd_forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_forms` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `created_by` smallint(5)  DEFAULT NULL,
  `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
  -- `results` text  DEFAULT NULL,
  `interpretation` text  DEFAULT NULL,
  `methodology` text  DEFAULT NULL,
  `labcode` varchar(255) DEFAULT NULL,
  `patientcode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  -- FULLTEXT `results` (`results`),
  FULLTEXT `interpretation` (`interpretation`),
  FULLTEXT `methodology` (`methodology`),
  CONSTRAINT `lovd_form_variants_fk_created_by` FOREIGN KEY (`created_by`) REFERENCES `lovd_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lovd_forms2variants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lovd_forms2variants` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `formvariantid` INT(10) NOT NULL,
  `formid` INT(10) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `formvariantid` (`formvariantid`),
  INDEX `formid` (`formid`),
  CONSTRAINT `lovd_form_variants_fk_formvariantid` FOREIGN KEY (`formvariantid`) REFERENCES `lovd_form_variants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lovd_form_variants_fk_formid` FOREIGN KEY (`formid`) REFERENCES `lovd_forms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS=1;


-- CREATE INDEX IF NOT EXISTS transcript_index ON lovd_transcripts (id);
-- CREATE INDEX IF NOT EXISTS gene_index ON lovd_genes (id);
-- CREATE FULLTEXT INDEX IF NOT EXISTS gene_fulltext_index ON lovd_genes(name);
-- CREATE INDEX IF NOT EXISTS AF_index ON lovd_variants (AF);
-- CREATE INDEX IF NOT EXISTS TDP_index ON lovd_variants (TDP);
-- CREATE INDEX IF NOT EXISTS FDP_index ON lovd_variants (FDP);
-- CREATE INDEX IF NOT EXISTS STB_index ON lovd_variants (STB);
-- CREATE INDEX IF NOT EXISTS ALTSTB_index ON lovd_variants (ALTSTB);
-- CREATE INDEX IF NOT EXISTS REFSTB_index ON lovd_variants (REFSTB);
-- CREATE INDEX IF NOT EXISTS FORWARDSTA_index ON lovd_variants (FORWARDSTA);
-- CREATE INDEX IF NOT EXISTS REVERSESTA_index ON lovd_variants (REVERSESTA);
-- CREATE INDEX IF NOT EXISTS NOSTA_index ON lovd_variants (NOSTA);
-- CREATE FULLTEXT INDEX IF NOT EXISTS variantongenomedna_fulltext_index ON lovd_variants (`VariantOnGenome/DNA`);

INSERT INTO `lovd_gene_panels` (`panel_name`)
VALUES 
('hereditary_cancers'),
('solid_tumors'),
('hematologic_malignancies');


INSERT INTO lovd_genes  (geneid, name) VALUES 
('ENSG00000142208', 'AKT1'), 
('ENSG00000147889', 'CDKN2A'), 
('ENSG00000182054', 'IDH2'),
('ENSG00000076242', 'MLH1'),
('ENSG00000171862', 'PTEN'),
('ENSG00000171094', 'ALK'),
('ENSG00000149554', 'CHEK1'),
('ENSG00000096968','JAK2'),
('ENSG00000095002', 'MSH2'),
('ENSG00000113522', 'RAD50'),
('ENSG00000169083','AR'),
('ENSG00000183765','CHEK2'),
('ENSG00000177606','JUN'),
('ENSG00000116062', 'MSH6'),
('ENSG00000051180','RAD51'),
('ENSG00000149311', 'ATM'),
('ENSG00000168036', 'CTNNB1'),
('ENSG00000128052', 'KDR'),
('ENSG00000198793', 'MTOR'),
('ENSG00000139687', 'RB1'),
('ENSG00000171791', 'BCL2'),
('ENSG00000146648', 'EGFR'),
('ENSG00000157404', 'KIT'),
('ENSG00000136997','MYC'),
('ENSG00000165731', 'RET'),
('ENSG00000157764', 'BRAF'),
('ENSG00000141736', 'ERBB2'),
('ENSG00000055609', 'KMT2C'),
('ENSG00000196712', 'NF1'),
('ENSG00000047936', 'ROS1'),
('ENSG00000012048', 'BRCA1'),
('ENSG00000178568', 'ERBB4'),
('ENSG00000133703', 'KRAS'),
('ENSG00000148400', 'NOTCH1'),
('ENSG00000141646', 'SMAD4'),
('ENSG00000139618', 'BRCA2'),
('ENSG00000091831', 'ESR1'),
('ENSG00000169032', 'MAP2K1'),
('ENSG00000213281', 'NRAS'),
('ENSG00000168610', 'STAT3'),
('ENSG00000110092','CCND1'),
('ENSG00000077782','FGFR1'),
('ENSG00000135679','MDM2'),
('ENSG00000083093', 'PALB2'),
('ENSG00000118046','STK11'),
('ENSG00000105173', 'CCNE1'),
('ENSG00000066468', 'FGFR2'),
('ENSG00000198625','MDM4'),
('ENSG00000134853', 'PDGFRA'),
('ENSG00000141510', 'TP53'),
('ENSG00000135446', 'CDK4'),
('ENSG00000068078', 'FGFR3'),
('ENSG00000133895', 'MEN1'),
('ENSG00000121879', 'PIK3CA'),
('ENSG00000105810', 'CDK6'),
('ENSG00000138413','IDH1'),
('ENSG00000105976', 'MET'),
('ENSG00000051382', 'PIK3CB'),
('ENSG00000134982','APC'),
('ENSG00000108384','RAD51C'),
('ENSG00000132781','MUTYH'),
('ENSG00000185379','RAD51D'),
('ENSG00000163930','BAP1'),
('ENSG00000100697','DICER1'),
('ENSG00000104320','NBN'),
('ENSG00000197299','BLM'),
('ENSG00000187790','FANCM'),
('ENSG00000160957','RECQL4'),
('ENSG00000165699','TSC1'),
('ENSG00000107779','BMPR1A'),
('ENSG00000091483','FH'),
('ENSG00000186575','NF2'),
('ENSG00000103197','TSC2'),
('ENSG00000154803','FLCN'),
('ENSG00000117118','SDHB'),
('ENSG00000134086','VHL'),
('ENSG00000064933','PMS1'),
('ENSG00000143252','SDHC'),
('ENSG00000136492','BRIP1'),
('ENSG00000122512','PMS2'),
('ENSG00000204370','SDHD'),
('ENSDARG00000102750','CDH1'),
('ENSG00000188827','SLX4'),
('ENSG00000171456','ASXL1'),
('ENSG00000121966','CXCR4'),
('ENSG00000185811','IKZF1'),
('ENSG00000181163','NPM1'),
('ENSG00000161547','SRSF2'),
('ENSG00000119772','DNMT3A'),
('ENSG00000030419','IKZF2'),
('ENSG00000101972','STAG2'),
('ENSG00000139083','ETV6'),
('ENSG00000161405','IKZF3'),
('ENSG00000196092','PAX5'),
('ENSG00000168769','TET2'),
('ENSG00000010671','BTK'),
('ENSG00000106462','EZH2'),
('ENSG00000162434','JAK1'),
('ENSG00000197943','PLCG2'),
('ENSG00000179218','CALR'),
('ENSG00000109670','FBXW7'),
('ENSG00000067560','RHOA'),
('ENSG00000160201','U2AF1'),
('ENSG00000110395','CBL'),
('ENSG00000122025','FLT3'),
('ENSG00000117400','MPL'),
('ENSG00000159216','RUNX1'),
('ENSG00000169249','ZRSR2'),
('ENSG00000245848','CEBPA'),
('ENSG00000172936','MYD88'),
('ENSG00000152217','SETBP1'),
('ENSG00000119535','CSF3R'),
('ENSG00000115524','SF3B1'),
('1029', 'CDKN2A'),
('11200','CHEK2'),
('6794','STK11'),
('5888','RAD51'),
('2956', 'MSH6'),
('4595', 'MUTYH'),
('472', 'ATM'),
('5889', 'RAD51C'),
('5979', 'RET'),
('641', 'BLM'),
('672', 'BRCA1'),
('7157', 'TP53'),
('83990', 'BRIP1'),
('10111', 'RAD50'),
('1019', 'CDK4'),
('1021', 'CDK6'),
('1111', 'CHEK1'),
('1499', 'CTNNB1'),
('1956', 'EGFR'),
('2064', 'ERBB2'),
('2066', 'ERBB4'),
('207', 'AKT1'),
('2099', 'ESR1'),
('2261', 'FGFR3'),
('2263', 'FGFR2'),
('238', 'ALK'),
('2475', 'MTOR'),
('3791', 'KDR'),
('3815', 'KIT'),
('3845', 'KRAS'),
('4089', 'SMAD4'),
('4193', 'MDM2'),
('4221', 'MEN1'),
('4233', 'MET'),
('4292', 'MLH1'),
('4436', 'MSH2'),
('4763', 'NF1'),
('5156', 'PDGFRA'),
('5290', 'PIK3CA'),
('5291', 'PIK3CB'),
('5604', 'MAP2K1'),
('5728', 'PTEN'),
('58508', 'KMT2C'),
('5925', 'RB1'),
('596', 'BCL2'),
('6098', 'ROS1'),
('675', 'BRCA2'),
('6774', 'STAT3'),
('79728', 'PALB2'),
('898', 'CCNE1'),
('10320', 'IKZF1'),
('1050', 'CEBPA'),
('10735', 'STAG2'),
('1441', 'CSF3R'),
('171023', 'ASXL1'),
('1788', 'DNMT3A'),
('2120', 'ETV6'),
('2146', 'EZH2'),
('22806', 'IKZF3'),
('22807', 'IKZF2'),
('2322', 'FLT3'),
('23451', 'SF3B1'),
('26040', 'SETBP1'),
('3417', 'IDH1'),
('3418', 'IDH2'),
('3716', 'JAK1'),
('3717', 'JAK2'),
('387', 'RHOA'),
('4352', 'MPL'),
('4615', 'MYD88'),
('4851', 'NOTCH1'),
('4869', 'NPM1'),
('4893', 'NRAS'),
('5079', 'PAX5'),
('5336', 'PLCG2'),
('54790', 'TET2'),
('55294', 'FBXW7'),
('6427', 'SRSF2'),
('673', 'BRAF'),
('695', 'BTK'),
('7307', 'U2AF1'),
('7852', 'CXCR4'),
('811', 'CALR'),
('861', 'RUNX1'),
('867', 'CBL'),
('999', 'CDH1'), --
('5378', 'PMS1'), --
('2271', 'FH'), --
('23405', 'DICER1'), --
('7249', 'TSC2'), --
('4683', 'NBN'), --
('657', 'BMPR1A'), --
('6390', 'SDHB'), --
('5395', 'PMS2'), -- 
('7428', 'VHL'), -- 
('324', 'APC'), --
('6391', 'SDHC'), --
('201163', 'FLCN'), --
('9401', 'RECQL4'), --
('8314', 'BAP1'), --
('7248', 'TSC1'), --
('5892', 'RAD51D'), --
('4771', 'NF2'), --
('57697', 'FANCM'), -- 
('84464', 'SLX4'), --
('6392', 'SDHD'), --
('2260', 'FGFR1'), --
('595', 'CCND1'), --
('4609', 'MYC'), --
('367', 'AR'), --
('3725', 'JUN'), --
('4194', 'MDM4'), --
('8233', 'ZRSR2'); --

INSERT INTO lovd_panels2genes  (geneid, panelid) VALUES
( 59 , 1 ),
( 2 , 1 ),
( 14 , 1 ),
( 60 , 1 ),
( 35 , 1 ),
( 16 , 1 ),
( 12 , 1 ),
( 61 , 1 ),
( 62 , 1 ),
( 45 , 1 ),
( 63 , 1 ),
( 64 , 1 ),
( 65 , 1 ),
( 20 , 1 ),
( 50 , 1 ),
( 66 , 1 ),
( 67 , 1 ),
( 29 , 1 ),
( 68 , 1 ),
( 69 , 1 ),
( 70 , 1 ),
( 71 , 1 ),
( 72 , 1 ),
( 25 , 1 ),
( 73 , 1 ),
( 31 , 1 ),
( 74 , 1 ),
( 44 , 1 ),
( 75 , 1 ),
( 76 , 1 ),
( 36 , 1 ),
( 53 , 1 ),
( 77 , 1 ),
( 78 , 1 ),
( 79 , 1 ),
( 4 , 1 ),
( 80 , 1 ),
( 81 , 1 ),
( 82 , 1 ),
( 9 , 1 ),
( 5 , 1 ),
( 83 , 1 ),
( 1 , 2 ),
( 2 , 2 ),
( 3 , 2 ),
( 4 , 2 ),
( 5 , 2 ),
( 6 , 2 ),
( 7 , 2 ),
( 8 , 2 ),
( 9 , 2 ),
( 10 , 2 ),
( 11 , 2 ),
( 12 , 2 ),
( 13 , 2 ),
( 14 , 2 ),
( 15 , 2 ),
( 16 , 2 ),
( 17 , 2 ),
( 18 , 2 ),
( 19 , 2 ),
( 20 , 2 ),
( 21 , 2 ),
( 22 , 2 ),
( 23 , 2 ),
( 24 , 2 ),
( 25 , 2 ),
( 26 , 2 ),
( 27 , 2 ),
( 28 , 2 ),
( 29 , 2 ),
( 30 , 2 ),
( 31 , 2 ),
( 32 , 2 ),
( 33 , 2 ),
( 34 , 2 ),
( 35 , 2 ),
( 36 , 2 ),
( 37 , 2 ),
( 38 , 2 ),
( 39 , 2 ),
( 40 , 2 ),
( 41 , 2 ),
( 42 , 2 ),
( 43 , 2 ),
( 44 , 2 ),
( 45 , 2 ),
( 46 , 2 ),
( 47 , 2 ),
( 48 , 2 ),
( 49 , 2 ),
( 50 , 2 ),
( 51 , 2 ),
( 52 , 2 ),
( 53 , 2 ),
( 54 , 2 ),
( 55 , 2 ),
( 56 , 2 ),
( 57 , 2 ),
( 58 , 2 ),
( 84 , 3 ),
( 85 , 3 ),
( 86 , 3 ),
( 87 , 3 ),
( 88 , 3 ),
( 16 , 3 ),
( 89 , 3 ),
( 90 , 3 ),
( 39 , 3 ),
( 91 , 3 ),
( 26 , 3 ),
( 92 , 3 ),
( 93 , 3 ),
( 94 , 3 ),
( 95 , 3 ),
( 96 , 3 ),
( 97 , 3 ),
( 98 , 3 ),
( 99 , 3 ),
( 50 , 3 ),
( 100 , 3 ),
( 101 , 3 ),
( 8 , 3 ),
( 102 , 3 ),
( 103 , 3 ),
( 104 , 3 ),
( 105 , 3 ),
( 106 , 3 ),
( 107 , 3 ),
( 108 , 3 ),
( 109 , 3 ),
( 56 , 3 ),
( 110 , 3 ),
( 111 , 3 ),
( 112 , 3 ),
( 3 , 3 ),
( 34 , 3 ),
( 113 , 3 ),
( 115 , 1 ),
( 116 , 1 ),
( 114 , 1 ),
( 118 , 1 ),
( 144 , 1 ),
( 146 , 1 ),
( 148 , 1 ),
( 149 , 1 ),
( 119 , 1 ),
( 120 , 1 ),
( 150 , 1 ),
( 155 , 1 ),
( 121 , 1 ),
( 157 , 1 ),
( 122 , 1 ),
( 123 , 1 ),
( 124 , 1 ),
( 160 , 1 ),
( 125 , 1 ),
( 162 , 1 ),
( 126 , 1 ),
( 117 , 2 ),
( 115 , 2 ),
( 116 , 2 ),
( 127 , 2 ),
( 128 , 2 ),
( 129 , 2 ),
( 114 , 2 ),
( 130 , 2 ),
( 131 , 2 ),
( 132 , 2 ),
( 133 , 2 ),
( 134 , 2 ),
( 135 , 2 ),
( 136 , 2 ),
( 137 , 2 ),
( 138 , 2 ),
( 139 , 2 ),
( 140 , 2 ),
( 118 , 2 ),
( 177 , 2 ),
( 178 , 2 ),
( 180 , 2 ),
( 141 , 2 ),
( 142 , 2 ),
( 143 , 2 ),
( 144 , 2 ),
( 145 , 2 ),
( 146 , 2 ),
( 147 , 2 ),
( 148 , 2 ),
( 149 , 2 ),
( 120 , 2 ),
( 150 , 2 ),
( 184 , 2 ),
( 186 , 2 ),
( 151 , 2 ),
( 152 , 2 ),
( 153 , 2 ),
( 154 , 2 ),
( 155 , 2 ),
( 156 , 2 ),
( 157 , 2 ),
( 158 , 2 ),
( 122 , 2 ),
( 159 , 2 ),
( 124 , 2 ),
( 192 , 2 ),
( 160 , 2 ),
( 161 , 2 ),
( 125 , 2 ),
( 162 , 2 ),
( 163 , 2 ),
( 164 , 3 ),
( 165 , 3 ),
( 166 , 3 ),
( 167 , 3 ),
( 168 , 3 ),
( 169 , 3 ),
( 170 , 3 ),
( 171 , 3 ),
( 172 , 3 ),
( 173 , 3 ),
( 174 , 3 ),
( 175 , 3 ),
( 176 , 3 ),
( 177 , 3 ),
( 178 , 3 ),
( 179 , 3 ),
( 180 , 3 ),
( 181 , 3 ),
( 182 , 3 ),
( 183 , 3 ),
( 120 , 3 ),
( 184 , 3 ),
( 185 , 3 ),
( 186 , 3 ),
( 187 , 3 ),
( 188 , 3 ),
( 189 , 3 ),
( 190 , 3 ),
( 191 , 3 ),
( 192 , 3 ),
( 193 , 3 ),
( 125 , 3 ),
( 194 , 3 ),
( 195 , 3 ),
( 196 , 3 ),
( 197 , 3 ),
( 198 , 3 ),
( 199 , 1 ),
( 200 , 1 ),
( 201 , 1 ),
( 202 , 1 ),
( 203 , 1 ),
( 204 , 1 ),
( 205 , 1 ),
( 206 , 1 ),
( 207 , 1 ),
( 208 , 1 ),
( 209 , 1 ),
( 210 , 1 ),
( 211 , 1 ),
( 212 , 1 ),
( 213 , 1 ),
( 214 , 1 ),
( 215 , 1 ),
( 216 , 1 ),
( 217 , 1 ),
( 218 , 1 ),
( 219 , 1 ),
( 220 , 2 ),
( 221 , 2 ),
( 222 , 2 ),
( 223 , 2 ),
( 224 , 2 ),
( 225 , 2 ),
( 226 , 3 ); 
-- ('ENSG00000134982',1),
-- ('ENSG00000147889', 1),
-- ('ENSG00000116062', 1),
-- ('ENSG00000108384',1),
-- ('ENSG00000141646', 1),
-- ('ENSG00000149311', 1),
-- ('ENSG00000183765',1),
-- ('ENSG00000132781',1),
-- ('ENSG00000185379',1),
-- ('ENSG00000118046',1),
-- ('ENSG00000163930',1),
-- ('ENSG00000100697',1),
-- ('ENSG00000104320',1),
-- ('ENSG00000139687',1),
-- ('ENSG00000141510', 1),
-- ('ENSG00000197299',1),
-- ('ENSG00000187790',1),
-- ('ENSG00000196712', 1),
-- ('ENSG00000160957',1),
-- ('ENSG00000165699',1),
-- ('ENSG00000107779',1),
-- ('ENSG00000091483',1),
-- ('ENSG00000186575',1),
-- ('ENSG00000165731', 1),
-- ('ENSG00000103197',1),
-- ('ENSG00000012048', 1),
-- ('ENSG00000154803',1),
-- ('ENSG00000083093', 1),
-- ('ENSG00000117118',1),
-- ('ENSG00000134086',1),
-- ('ENSG00000139618', 1),
-- ('ENSG00000133895', 1),
-- ('ENSG00000064933',1),
-- ('ENSG00000143252',1),
-- ('ENSG00000136492',1),
-- ('ENSG00000076242', 1),
-- ('ENSG00000122512',1),
-- ('ENSG00000204370',1),
-- ('ENSDARG00000102750',1),
-- ('ENSG00000095002', 1),
-- ('ENSG00000171862', 1),
-- ('ENSG00000188827',1),
-- ('ENSG00000142208', 2),
-- ('ENSG00000147889', 2),
-- ('ENSG00000182054', 2),
-- ('ENSG00000076242', 2),
-- ('ENSG00000171862', 2),
-- ('ENSG00000171094', 2),
-- ('ENSG00000149554', 2),
-- ('ENSG00000096968',2),
-- ('ENSG00000095002', 2),
-- ('ENSG00000113522', 2),
-- ('ENSG00000169083',2),
-- ('ENSG00000183765',2),
-- ('ENSG00000177606',2),
-- ('ENSG00000116062', 2),
-- ('ENSG00000051180',2),
-- ('ENSG00000149311', 2),
-- ('ENSG00000168036', 2),
-- ('ENSG00000128052', 2),
-- ('ENSG00000198793', 2),
-- ('ENSG00000139687', 2),
-- ('ENSG00000171791', 2),
-- ('ENSG00000146648', 2),
-- ('ENSG00000157404', 2),
-- ('ENSG00000136997',2),
-- ('ENSG00000165731', 2),
-- ('ENSG00000157764', 2),
-- ('ENSG00000141736', 2),
-- ('ENSG00000055609', 2),
-- ('ENSG00000196712', 2),
-- ('ENSG00000047936', 2),
-- ('ENSG00000012048', 2),
-- ('ENSG00000178568', 2),
-- ('ENSG00000133703', 2),
-- ('ENSG00000148400', 2),
-- ('ENSG00000141646', 2),
-- ('ENSG00000139618', 2),
-- ('ENSG00000091831', 2),
-- ('ENSG00000169032', 2),
-- ('ENSG00000213281', 2),
-- ('ENSG00000168610', 2),
-- ('ENSG00000110092',2),
-- ('ENSG00000077782',2),
-- ('ENSG00000135679',2),
-- ('ENSG00000083093', 2),
-- ('ENSG00000118046',2),
-- ('ENSG00000105173', 2),
-- ('ENSG00000066468', 2),
-- ('ENSG00000198625',2),
-- ('ENSG00000134853', 2),
-- ('ENSG00000141510', 2),
-- ('ENSG00000135446', 2),
-- ('ENSG00000068078', 2),
-- ('ENSG00000133895', 2),
-- ('ENSG00000121879', 2),
-- ('ENSG00000105810', 2),
-- ('ENSG00000138413',2),
-- ('ENSG00000105976', 2),
-- ('ENSG00000051382', 2),
-- ('ENSG00000171456',3),
-- ('ENSG00000121966',3),
-- ('ENSG00000185811',3),
-- ('ENSG00000181163',3),
-- ('ENSG00000161547',3),
-- ('ENSG00000149311',3),
-- ('ENSG00000119772',3),
-- ('ENSG00000030419',3),
-- ('ENSG00000213281', 3),
-- ('ENSG00000101972',3),
-- ('ENSG00000157764', 3),
-- ('ENSG00000139083',3),
-- ('ENSG00000161405',3),
-- ('ENSG00000196092',3),
-- ('ENSG00000168769',3),
-- ('ENSG00000010671',3),
-- ('ENSG00000106462',3),
-- ('ENSG00000162434',3),
-- ('ENSG00000197943',3),
-- ('ENSG00000141510', 3),
-- ('ENSG00000179218',3),
-- ('ENSG00000109670',3),
-- ('ENSG00000096968',3),
-- ('ENSG00000067560',3),
-- ('ENSG00000160201',3),
-- ('ENSG00000110395',3),
-- ('ENSG00000122025',3),
-- ('ENSG00000117400',3),
-- ('ENSG00000159216',3),
-- ('ENSG00000169249',3),
-- ('ENSG00000245848',3),
-- ('ENSG00000138413',3),
-- ('ENSG00000172936',3),
-- ('ENSG00000152217',3),
-- ('ENSG00000119535',3),
-- ('ENSG00000182054', 3),
-- ('ENSG00000148400', 3),
-- ('ENSG00000115524',3),
-- ('11200', 1),
-- ('6794', 1),
-- ('1029', 1),
-- ('2956', 1),
-- ('4089', 1),
-- ('4221', 1),
-- ('4292', 1),
-- ('4436', 1),
-- ('4595', 1),
-- ('472', 1),
-- ('4763', 1),
-- ('5728', 1),
-- ('5889', 1),
-- ('5925', 1),
-- ('5979', 1),
-- ('641', 1),
-- ('672', 1),
-- ('675', 1),
-- ('7157', 1),
-- ('79728', 1),
-- ('83990', 1),
-- ('5888', 2),
-- ('11200', 2),
-- ('6794', 2),
-- ('10111', 2),
-- ('1019', 2),
-- ('1021', 2),
-- ('1029', 2),
-- ('1111', 2),
-- ('1499', 2),
-- ('1956', 2),
-- ('2064', 2),
-- ('2066', 2),
-- ('207', 2),
-- ('2099', 2),
-- ('2261', 2),
-- ('2263', 2),
-- ('238', 2),
-- ('2475', 2),
-- ('2956', 2),
-- ('3417', 2),
-- ('3418', 2),
-- ('3717', 2),
-- ('3791', 2),
-- ('3815', 2),
-- ('3845', 2),
-- ('4089', 2),
-- ('4193', 2),
-- ('4221', 2),
-- ('4233', 2),
-- ('4292', 2),
-- ('4436', 2),
-- ('472', 2),
-- ('4763', 2),
-- ('4851', 2),
-- ('4893', 2),
-- ('5156', 2),
-- ('5290', 2),
-- ('5291', 2),
-- ('5604', 2),
-- ('5728', 2),
-- ('58508', 2),
-- ('5925', 2),
-- ('596', 2),
-- ('5979', 2),
-- ('6098', 2),
-- ('672', 2),
-- ('673', 2),
-- ('675', 2),
-- ('6774', 2),
-- ('7157', 2),
-- ('79728', 2),
-- ('898', 2),
-- ('10320', 3),
-- ('1050', 3),
-- ('10735', 3),
-- ('1441', 3),
-- ('171023', 3),
-- ('1788', 3),
-- ('2120', 3),
-- ('2146', 3),
-- ('22806', 3),
-- ('22807', 3),
-- ('2322', 3),
-- ('23451', 3),
-- ('26040', 3),
-- ('3417', 3),
-- ('3418', 3),
-- ('3716', 3),
-- ('3717', 3),
-- ('387', 3),
-- ('4352', 3),
-- ('4615', 3),
-- ('472', 3),
-- ('4851', 3),
-- ('4869', 3),
-- ('4893', 3),
-- ('5079', 3),
-- ('5336', 3),
-- ('54790', 3),
-- ('55294', 3),
-- ('6427', 3),
-- ('673', 3),
-- ('695', 3),
-- ('7157', 3),
-- ('7307', 3),
-- ('7852', 3),
-- ('811', 3),
-- ('861', 3),
-- ('867', 3),
-- ('999', 1), --
-- ('5378', 1), --
-- ('2271', 1), --
-- ('23405', 1), --
-- ('7249', 1),--
-- ('4683', 1), --
-- ('657', 1), --
-- ('6390', 1), --
-- ('5395', 1), -- 
-- ('7428', 1), -- 
-- ('324', 1), --
-- ('6391', 1), --
-- ('201163', 1), --
-- ('9401', 1), --
-- ('8314', 1), --
-- ('7248', 1), --
-- ('5892', 1), --
-- ('4771', 1), --
-- ('57697', 1), -- 
-- ('84464', 1), --
-- ('6392', 1), --
-- ('2260', 2), --
-- ('595', 2), --
-- ('4609', 2), --
-- ('367', 2), --
-- ('3725', 2), --
-- ('4194', 2), --
-- ('8233', 3); --